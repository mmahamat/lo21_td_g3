#include <QApplication>
#include <QPushButton>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    QPushButton a_button("ok");
    QPushButton a_second_button("hi");
    QObject::connect(&a_button,
                     &QPushButton::clicked,
                     &app,
                     &QApplication::quit);
//  QObject::connect(&a_button,
//                   SIGNAL(clicked()),
//                   &app,
//                   SLOT(quit()));
    a_button.show();
    //a_second_button.show();
    return app.exec();
}
