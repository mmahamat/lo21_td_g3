#if !defined(LOG_H)
#define LOG_H
#include "timing.h"
#include<iostream>
#include "evenement.h"
#include <exception>
class Log {
public:
	virtual void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string
		& s) = 0;
	virtual void displayLog(std::ostream& f) const = 0;
};

class MyLogClass : public Log, private TIME::Agenda
{
public: 
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string
		& s) override;
	void displayLog(std::ostream& f) const override;
};

class MyLogObject : public Log
{
private:

	TIME::Agenda log_events;
public:
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string
		& s) override;
	void displayLog(std::ostream& f) const override;
};

class LogError : public std::exception
{
private:
	std::string m_error;
public:
	LogError(const char* s_error) noexcept : m_error(s_error) {};
	const char* what() const noexcept override { return m_error.c_str(); }
	// noexcept : la methode ne declenchera pas d'exception

};

#endif