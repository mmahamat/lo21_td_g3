#include "log.h"
#include "evenement.h"
#include <string>
void MyLogClass::addEvt(const TIME::Date& d, 
	const TIME::Horaire& h, const std::string& s)
{
	if (begin() != end())
	{
		const TIME::Evt1jDur& last_event = dynamic_cast<const TIME::Evt1jDur&>(**(--end()));
		if (d < last_event.getDate() ||
			(d == last_event.getDate() && h < last_event.getHoraire()))
		{
			throw LogError("Ajout d'un event anterieur impossible");
		}
	}
	//verifier si on ajoute pas un event anterieur
	*this << TIME::Evt1jDur(d, s, h, TIME::Duree(0));
}

void MyLogClass::displayLog(std::ostream& f) const
{
	for (Agenda::const_iterator it = begin();
		it != end(); ++it)
	{
		f << dynamic_cast<const TIME::Evt1jDur&>(*it).getDate()
			<< " -----" << dynamic_cast<const TIME::Evt1jDur&>(*it).getHoraire()
			<< "------\n event happened: "
			<< dynamic_cast<const TIME::Evt1jDur&>(*it).getDescription() << "\n";
	}

}

//adapteur d'objet:

void MyLogObject::addEvt(const TIME::Date& d,
	const TIME::Horaire& h, const std::string& s)
{
	if (log_events.begin() != log_events.end())
	{
		const TIME::Evt1jDur& last_event = dynamic_cast<const TIME::Evt1jDur&>(**(--log_events.end()));
		if (d < last_event.getDate() ||
			(d == last_event.getDate() && h < last_event.getHoraire()))
		{
			throw LogError("Ajout d'un event anterieur impossible");
		}
	}
	//verifier apres si on a le respect de l'anteriorite
	log_events << TIME::Evt1jDur(d, s, h, TIME::Duree(0));

}

void MyLogObject::displayLog(std::ostream& f) const
{
	for (TIME::Agenda::const_iterator it = log_events.begin();
		it != log_events.end(); ++it)
	{
		f << dynamic_cast<const TIME::Evt1jDur&>(*it).getDate()
			<< " -----" << dynamic_cast<const TIME::Evt1jDur&>(*it).getHoraire()
			<< "------\n event happened: "
			<< dynamic_cast<const TIME::Evt1jDur&>(*it).getDescription() << "\n";
	}

}