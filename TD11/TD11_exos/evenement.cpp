#include "evenement.h"

//void TIME::EvtPj::afficher(std::ostream& f) const
//{
//
//	/* Afficher les dates de debut et de fin*/
//	f << "***************Evenement sur plusieurs jours******************" << "\n";
//	f << "sujet=" << getDescription() << "\n";
//	f << "date de debut = " << dates_event.getDebut()<< "\n";
//	f << "date de fin = " << dates_event.getFin() << "\n";
//}
//
//void TIME::Evt1j::afficher(std::ostream&f ) const {
//	f << "***** Evt sur 1j ********" << "\n" << "Date=" << date << " sujet=" << getDescription() << "\n";
//}
//
//void TIME::Evt1jDur::afficher(std::ostream& f) const {
//	Evt1j::afficher(f);
//	f << "horaire de debut: " << getHoraire() << ", duree : " << getDuree() << "\n";
//}
//
//void TIME::Rdv::afficher(std::ostream&f) const
//{
//	Evt1jDur::afficher(f);
//	f << "personnes = " << getPersonnes() << ", lieu = " << getLieu() << "\n";
//}
#include <sstream>
std::string TIME::Evt1j::toString() const
{
	std::stringstream t_s;
	t_s<< "***** Evt sur 1j ********" << "\n" << "Date=" << date << " sujet=" << getDescription() << "\n";
	return t_s.str();
}

std::string TIME::EvtPj::toString() const
{
	std::stringstream t_s;
	t_s << "***************Evenement sur plusieurs jours******************" << "\n";
	t_s <<"sujet=" << getDescription() << "\n";
	t_s << "date de debut = " << dates_event.getDebut()<< "\n";
	t_s << "date de fin = " << dates_event.getFin() << "\n";
	return t_s.str();
}

std::string TIME::Evt1jDur::toString() const
{
	std::stringstream t_s;
	t_s<< Evt1j::toString()<< "horaire de debut: " 
		<< getHoraire() << ", duree : " << getDuree() << "\n"; 
	return t_s.str();
}

std::string TIME::Rdv::toString() const
{
	std::stringstream t_s;
	t_s << Evt1jDur::toString() << "personnes = " 
		<< getPersonnes() << ", lieu = " << getLieu() << "\n";
	return t_s.str();
}

TIME::Evt1j* TIME::Evt1j::duplicate() const
{

	return new Evt1j(*this);
}

TIME::EvtPj* TIME::EvtPj::duplicate() const
{

	return new EvtPj(*this);
}

TIME::Evt1jDur* TIME::Evt1jDur::duplicate() const
{

	return new Evt1jDur(*this);
}

TIME::Rdv* TIME::Rdv::duplicate() const
{
	 
	return new Rdv(*this);
}

TIME::Agenda& TIME::Agenda::operator<<(const TIME::Evt& e)
{
	m_events.push_back(e.duplicate()); //stockage de l'adresse
	return *this; //retour de l'agenda
}

void TIME::Agenda::afficher(std::ostream& f) const
{
	f << "Voici votre agenda:\n";
	f << "----------------------\n";
	for (size_t i = 0; i < m_events.size(); i++)
		m_events[i]->afficher();
	f << "---------------------\n";
	f << "Fin de votre agenda.\n";
}

std::ostream& operator<<(std::ostream& f, const TIME::Evt& t_evt)
{
	t_evt.afficher(f);
	return f;
}

