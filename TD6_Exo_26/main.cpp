#include <QApplication>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <QHBoxLayout>
#include <QVBoxLayout>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QWidget fenetre;
//    fenetre.setFixedSize(250, 120);
//    fenetre.setMaximumSize(600,600);
    fenetre.setMinimumSize(300,300);
    QLabel *noml = new QLabel("Nom",&fenetre);
    QLineEdit* nom = new QLineEdit(&fenetre);
    QPushButton *ok = new QPushButton("ok",&fenetre);

    QHBoxLayout *hlayout = new QHBoxLayout;
    QVBoxLayout *vlayout = new QVBoxLayout;
    //ajout des widgets aux layouts

    hlayout->addWidget(noml);
    hlayout->addWidget(nom);

    vlayout->addLayout(hlayout);
    vlayout->addWidget(ok);
    //positionnement statique:
//    noml->move(10,10);
//    nom->move(60,10);
//    ok->move(10,60);

    //modification taille
    nom->setFixedWidth(180);
//    ok->setFixedWidth(230);

    //...
    //ajouter un layout a une interface:
    fenetre.setLayout(vlayout);
    fenetre.setWindowTitle("Joueur");
    fenetre.show();
    return app.exec();
}
