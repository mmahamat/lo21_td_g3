#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QProgressBar>
#include <QLCDNumber>
#include <QMessageBox>
#include "vuecarte.h"
#include "vuepartie.h"

VuePartie::VuePartie(QWidget *parent):QWidget(parent),
    vuecartes(20, nullptr)
{
 ///choses a faire:
 /// a) faire votre layout avec les infos de la partie
 /// dans un QHBoxLayout
 /// b) creer le layout des vuecartes avec
 /// un QGridLayout
 /// b)i) creer les VueCarte
 /// b)ii) connecter les VueCarte au slot carteClique de
 /// notre interface
 /// c) faire le layout final
 ///

    //Layout d'informations
    setWindowTitle("SET!");
    controleur.distribuer();
    pioche = new QLabel("pioche");
    score = new QLabel("score");
    scoreJoueur = new QLCDNumber;
    nbCartesPioche = new QProgressBar;
    nbCartesPioche->setRange(0,
                             Set::Jeu::getInstance().getNbCartes());
    nbCartesPioche->setValue(controleur.getPioche().getNbCartes());
    nbCartesPioche->setTextVisible(false);

    scoreJoueur->display(0);
    //peut-etre modifier taille des widgets, a voir
    //le resultat final
    layoutInformations = new QHBoxLayout;
    layoutInformations->addWidget(pioche);
    layoutInformations->addWidget(nbCartesPioche);
    layoutInformations->addWidget(score);
    layoutInformations->addWidget(scoreJoueur);
    //fin du layoutInformations

    //definir nos vueCartes et notre layout de
    //cartes
    for(size_t i =0; i<20; i++)
        vuecartes[i] = new VueCarte;

    layoutCartes = new QGridLayout;
    for(size_t i= 0;i<20;i++)
    {
        //ajout au layout des cartes et connection
        //du signal emis au slot carteClique
        layoutCartes->addWidget(vuecartes[i],
                                i/4,
                                i%4);
        connect(vuecartes[i],
                &VueCarte::carteClicked,
                this,
                &VuePartie::carteClique);
    }

    size_t taille_iterable = 0;
    for(auto it = controleur.getPlateau().begin();
        it != controleur.getPlateau().end();
        ++it)
    {
      //parcours des cartes du plateau, et ajout de chaque
      //carte a une vueCarte
    vuecartes[taille_iterable]->setCarte(*it);
    taille_iterable++;
    }

    //Creation du layout final pour l'interface
    couche = new QVBoxLayout;
    couche->addLayout(layoutInformations);
    couche->addLayout(layoutCartes);
    //afficher le layout commun
    setLayout(couche);
}

//slot carteClique
void VuePartie::carteClique(VueCarte* vc)
{
///votre slot doit gerer:
/// a)Si pas de carte presente dans vuecartes[i]:
/// a)i) si pioche vide: message warning QMessageBox
/// a)ii) sinon: piocher une carte et l'ajouter
/// b) si carte presente dans vuecartes[i]
/// b)i) soit on la selectionne, ajout dans selectionCartes
/// b)ii) si deja selectionnee, la deselectionner
/// b)iii) si 3 cartes dans selectionCartes: tester le SET
/// b)iv) si SET : augmenter le score, retirer les cartes
/// du plateau et mettre a jour l'interface
/// b)v) si non SET: vider la selection de cartes
/// et les deselectionner
/// c) dans tous les cas: mettre a jour l'interface
/// avec update()

    if(!vc->cartePresente())
    {
        if(controleur.getPioche().getNbCartes()==0)
        {
            QMessageBox message_warning(QMessageBox::Icon::Warning,
                                        "Attention...",
                                        "Pioche vide ...");
            message_warning.exec();
        }
        else
        {
            controleur.distribuer();
            size_t taille_iterable = 0;
            for(auto it = controleur.getPlateau().begin();
                it != controleur.getPlateau().end();
                ++it)
            {
              //parcours des cartes du plateau, et ajout de chaque
              //carte a une vueCarte
            vuecartes[taille_iterable]->setCarte(*it);
            taille_iterable++;
            }
            nbCartesPioche->setValue(controleur.getPioche().getNbCartes());
        }
    }
    //si au contraire, on a une carte presente dans
    //la vuecarte vc
    else
    {
        //selectionne une carte:
        if(vc->isChecked())
        {
            selectionCartes.insert(&vc->getCarte());
            if(selectionCartes.size()== 3) //tester si c'est un set
            {
                vector<const Set::Carte*> selection(selectionCartes.begin(),
                                                    selectionCartes.end());
                Set::Combinaison combi(*selection[0],
                        *selection[1],
                        *selection[2]); //la combinaison a tester
                if(combi.estUnSET()) // si on a un set
                {
                    //bravo, c'est un set
                    //on les retire du plateau
                    controleur.getPlateau().retirer(*selection[0]);
                    controleur.getPlateau().retirer(*selection[1]);
                    controleur.getPlateau().retirer(*selection[2]);

                    //clear la selection de cartes
                    selectionCartes.clear();
                    //si on a moins de 12 cartes sur le plateau
                    //on doit redistribuer des cartes
                    if(controleur.getPlateau().getNbCartes() <12)
                        controleur.distribuer();

                    //mise a jour du score du joueur
                    scoreValue++;
                    //mise a jour de l'affichage corrspondant
                    scoreJoueur->display(scoreValue);

                    //nettoyer l'interface et remettre nos cartes
                    for(size_t i=0; i<vuecartes.size(); i++)
                    {
                        vuecartes[i]->setNoCarte();
                    }
                    //remmetre les cartes sur l'interface
                    //(cacher les gaps des cartes enlevees)
                    size_t taille_iterable = 0;
                    for(auto it = controleur.getPlateau().begin();
                        it != controleur.getPlateau().end();
                        ++it)
                    {
                      //parcours des cartes du plateau, et ajout de chaque
                      //carte a une vueCarte
                    vuecartes[taille_iterable]->setCarte(*it);
                    taille_iterable++;
                    }
                    nbCartesPioche->setValue(controleur.getPioche().getNbCartes());
                }
                else //ce n'est pas un set:
                {
                    QMessageBox message_non_set(QMessageBox::Icon::Warning,
                                                "Attention...",
                                                "Ce n'est pas un set...");
                    message_non_set.exec();
                    //deselectionner les cartes:
                    for(size_t i =0; i< vuecartes.size();i++)
                        vuecartes[i]->setChecked(false);
                    //on nettoie la selection
                    selectionCartes.clear();
                }

            }

        }
        else //on doit deselectionner la carte:
        {
            selectionCartes.erase(&vc->getCarte());
        }
    }
    update(); //met a jour l'interface
}
