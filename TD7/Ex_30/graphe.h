#if !defined(_GRAPH_H)
#define _GRAPH_H
#include<string>
#include<stdexcept>
using namespace std;
class GraphException : public exception {
	string info;
public:
	GraphException(const string& i) noexcept :info(i) {}
	virtual ~GraphException() noexcept {}
	const char* what() const noexcept { return info.c_str(); }
};

#include<list>
#include<vector>
#include<iostream>
#include<string>

class Graph {
	vector<list<unsigned int> > adj;
	string name;
	//unsigned int nb_edges;
	size_t nb_edges; //plus facile pour traquer le nb d'aretes
public:
	Graph(const string& n, size_t nb) :name(n), nb_edges(0), adj(nb) {};
	/*on cree nb listes doubles chainees vides avec adj(nb) */
	const string& getName() const {
		return name;
	};
	size_t getNbVertices() const { return adj.size(); };
	size_t getNbEdges() const { return nb_edges; };
	void addEdge(unsigned int i, unsigned int j);
	void removeEdge(unsigned int i, unsigned int j);
	//size_t calculNbEdges() const; //possible d'avoir une methode
	//pour calculer "continuellement" le nombre d'aretes
	const list<unsigned int>& getSuccessors(unsigned int i) const;
	const list<unsigned int> getPredecessors(unsigned int i) const;
};
ostream& operator<<(ostream& f, const Graph& G);
#endif
