#include "graphe.h"

void Graph::addEdge(unsigned int i, unsigned int j)
{
	//choses a verifier:
	//verifier que les sommets sont valides [0,nb-1]
	//verifier que l'arete n'existe pas deja
	if (i >= getNbVertices() || j >= getNbVertices())
		throw GraphException("sommet non valide pour i ou j");
	/*list<unsigned int>::iterator t_it = find(adj[i].begin(),
		adj[i].end(), j);
	if (t_it != adj[i].end())*/
	if(find(adj[i].begin(),
		adj[i].end(), j) != adj[i].end())
	{
		throw GraphException("arete deja definie");
	}
	//adj[i].push_back(j);
	adj[i].insert(adj[i].end(), j);
	nb_edges++;
}

void Graph::removeEdge(unsigned int i, unsigned int j)
{
	//choses a verifier:
	//verifier que les sommets sont valides [0,nb-1]
	//verifier que l'arete existe dans le graphe
	if (i >= getNbVertices() || j >= getNbVertices())
		throw GraphException("sommet non valide pour i ou j");
	list<unsigned int>::iterator t_it = find(adj[i].begin(),
		adj[i].end(), j);
	if (t_it != adj[i].end())
	{
		adj[i].erase(t_it);
		nb_edges--;
	}
	else // arete inexistante
	{
		throw GraphException("arete inexistante");
	}

}

const list<unsigned int>& Graph::getSuccessors(unsigned int i) const
{
	return adj[i];
}

const list<unsigned int> Graph::getPredecessors(unsigned int i) const
{
	if (i >= getNbVertices())
		throw GraphException("sommet non valide");
	list<unsigned int> predecesseurs;
	for (unsigned int pos = 0; pos <getNbVertices(); pos++)
	{
		if (find(adj[pos].begin(), adj[pos].end(), i) != adj[pos].end())
		{
			predecesseurs.push_back(pos);
		}
	}
	return predecesseurs;

}

ostream& operator<<(ostream& f, const Graph& G)
{
	f << "graph " << G.getName() << " (" << G.getNbVertices()
		<< " vertices and " << G.getNbEdges() << " edges)\n";
	//affichage pour chaque sommet de la liste des successeurs
	for (size_t i = 0; i < G.getNbVertices(); i++)
	{
		f << i << ":";
		//afficher les successeurs;
		for (/*list<unsigned int>::const_iterator*/
			auto t_it = G.getSuccessors(i).begin();
			t_it != G.getSuccessors(i).end(); ++t_it)
		{
			f << *t_it <<",";
		}
		f << "\n";
	}
	return f;
}