#include <iostream>
#include "evenement.h"
int main() {
	using namespace std;
	using namespace TIME;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	e1.afficher();
	e2.afficher();
	{ // d�but de bloc
			//creation dans l'ordre Evt1j->Evt1jDur->Rdv

		Rdv e(Date(11, 11, 2013),
			"reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
			std::cout << "RDV:";
		e.afficher();
		//destruction dans l'ordre Rdv -> Evt1jdur-> Evt1j
	} // fin de bloc
	//nouveau bloc
	{ //probleme de polymorphisme avec le code fait en td ...
		Evt1j* my_pt1 = new Rdv(Date(26, 04, 2022),
			"reunion LO21", Horaire(11, 14), Duree(12), "Intervenants UV", "bureau");
		std::cout << "RDV pointe:";
		my_pt1->afficher();
		delete my_pt1;
	}

	return 0;
}