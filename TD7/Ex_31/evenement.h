#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H
#include <iostream>
#include <string>
#include "timing.h"
namespace TIME {
	class Evt1j {
	private:
		Date date;
		std::string sujet;
	public:
		Evt1j(const Date& d, const std::string& s) :date(d), sujet(s) 
		{ std::cout << "construction evt1j\n"; }
		~Evt1j() { std::cout << "destruction evt1j\n"; }
		const std::string& getDescription() const { return sujet; }
		const Date& getDate() const { return date; }
		void afficher(std::ostream& f = std::cout) const {
			f << "***** Evt ********" << "\n" << "Date=" << date << " sujet=" << sujet << "\n";
		}
	};


	class Evt1jDur : public Evt1j
	{
		Horaire m_horaire;
		Duree m_duree;

	public: 
		Evt1jDur(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur) :Evt1j(d, s),m_horaire(h), m_duree(dur)
		{
			std::cout << "evt1jdur\n";
		};
		~Evt1jDur() { std::cout << "destruction evt1jdur\n"; }
		const Horaire& getHoraire() const {
			return m_horaire;
		}
		const Duree& getDuree() const {
			return m_duree;
		}
		void afficher(std::ostream& f = std::cout) const {
			Evt1j::afficher(f);
			f << "horaire de debut: " << getHoraire() << ", duree : " << getDuree() << "\n";
		}
	};


	class Rdv : public Evt1jDur
	{
		std::string lieu;
		std::string personnes;
	public:
		Rdv(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur,
			const std::string& pers,
			const std::string& li) : Evt1jDur(d, s, h, dur), lieu(li), personnes(pers)
		{
			std::cout << "Rdv\n";
		};
		~Rdv() { std::cout << "destruction Rdv\n"; }
		const std::string& getLieu() const {
			return lieu;
		}
		const std::string& getPersonnes() const {
			return personnes;
		}
		void afficher(std::ostream& f = std::cout) const
		{
			Evt1jDur::afficher(f);
			f << "personnes = " << getPersonnes() << ", lieu = " << getLieu() << "\n";
		}
	};

}
#endif