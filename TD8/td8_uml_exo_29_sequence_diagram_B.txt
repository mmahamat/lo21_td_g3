@startuml
!theme reddress-lightred
skinparam sequenceParticipant underline

actor User as user
activate user
participant pile as "p: pile"
participant cs as "cs: carteSommaire"
collections btchaps as ":boutonChapitre"
participant btlien as "b1: boutonLien"
participant c1 as "carte1: Carte"
participant btlienv2 as "b2: boutonLien"
participant c2 as "carte2: Carte"

user -> pile : exec()
activate pile
pile -> cs : afficher()
activate cs
cs -> btchaps : afficher()
activate btchaps 
activate btlien
deactivate btchaps
cs-> btlien: afficher()
deactivate cs
deactivate pile
user -> btlien: onClick()
activate cs
btlien -> cs: manageClick(b1)
cs -> btlien: intit:=getIntitule()

alt intit = 'suivant'
   cs -> cs : carte1 := getSuivant()
   activate c1
   cs -> c1 : afficher()
end

c1 -> btlienv2 : afficher()
activate btlienv2 
deactivate cs
deactivate btlien
deactivate c1
user ->btlienv2 : onClick()

btlienv2 -> c1 : manageClick(b2)
activate c1
c1 -> c1 : intit2 := getIntitule()

alt intit2 = 'suivant'
   deactivate btlienv2
   c1 -> c2:afficher()
activate c2
end
deactivate c1
@enduml