#include "Fraction.h"
#include <iostream>	
//int main()
//{
//	MATH::Fraction f1(18,3); // f1(18,3)
//	MATH::Fraction f3(); // f3(0,1)
//	//f1.denominateur = 1;
//	//f1.numerateur = 1;
//	/*f1.setDenominateur(1);
//	f1.setNumerateur(2);*/
//	std::cout << "f1.num = " << f1.getNumerateur()
//		<< ", f1.den=" << f1.getDenominateur()<< "\n";
//	f1.setFraction(12, 6);
//	std::cout << "f1.num = " << f1.getNumerateur()
//		<< ", f1.den=" << f1.getDenominateur() << "\n";
//	MATH::Fraction f2(1,2);
//	MATH::Fraction fra = f1.somme(f2); //fra = f1.somme(f2);
//	std::cout << "fra.num = " << fra.getNumerateur()
//		<< ", fra.den=" << fra.getDenominateur() << "\n";
//	return 0;
//}

using namespace std;
using namespace MATH;
Fraction* myFunction() {
	Fraction fx(7, 8);
	Fraction* pfy = new Fraction(2, 3);
	return pfy;
}
int main() {
	Fraction f1(3, 4);
	Fraction f2(1, 6);
	Fraction* pf3 = new Fraction(1, 2);
	cout << "ouverture d置n bloc\n";
	Fraction* pf6;
	{
		Fraction f4(3, 8);
		Fraction f5(4, 6);
		pf6 = new Fraction(1, 3);
	}
	cout << "fin d置n bloc\n";
	cout << "debut d置ne fonction\n";
	Fraction* pf7 = myFunction();
	cout << "fin d置ne fonction\n";
	cout << "desallocations controlee par l置tilisateur :\n";
	delete pf6;
	delete pf7;


	Fraction my_f1(1, 2);
	Fraction my_f2(3, 4);
	Fraction my_f3 = my_f1 + my_f2; //montrer la surcharge de l'operator+
	Fraction a_random_fraction = somme(my_f1, my_f2);
	Fraction another_fraction_v2 = my_f1.somme(my_f2);
	cout <<"arandom="<< a_random_fraction;

	cout<< "my_f3.num = "<<my_f3.getNumerateur() <<", my_f3.den = "
		<< my_f3.getDenominateur() <<"\n";
	
	my_f3 = my_f1 + 1;
	cout << "my_f3.num plus1 = " << my_f3.getNumerateur()
		<< ", my_f3.den plus 1= "
		<< my_f3.getDenominateur() << "\n";

	my_f3 = 1+ my_f1; //erreur car on ferait 1.operator+(my_f1)
	cout << "my_f3.num plus1 = " << my_f3.getNumerateur()
		<< ", my_f3.den plus 1= "
		<< my_f3.getDenominateur() << "\n";

	cout << my_f3; //pour montrer la surcharge de l'ecriture dans un flux de type ostream
	
	Fraction my_bad_fraction(1, 1);
	//essai pour declencher une exception, avec une exception de type FractionException
	try
	{
		my_bad_fraction.setFraction(1, 0);
	}
	catch(FractionException& fraction_error)
	{
		std::cout << fraction_error.getInfo();
	}




	return 0;
}

