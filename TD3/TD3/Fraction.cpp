#include "Fraction.h"
#include <iostream>
void MATH::Fraction::setFraction(int n, int d)
{
	numerateur = n;
	denominateur = d;
	if (d == 0)
	{
		//std::cout << "denominateur ne peut pas etre nul!\n";
		throw MATH::FractionException("setFraction: denominateur ne peut pas etre nul!\n");
		denominateur = 1; //forcer denominateur egal a 1
	}
	MATH::Fraction::simplification();
}
void MATH::Fraction::setNumerateur(int n)
{
	numerateur = n;
}

void MATH::Fraction::setDenominateur(int d)
{
	denominateur = d;
	if (d == 0)
	{
		//std::cout << "denominateur ne peut pas etre nul!\n";
		throw MATH::FractionException("setDenominateur: denominateur ne peut pas etre nul!\n");
		denominateur = 1;

	}
}

void MATH::Fraction::simplification() {
	// si le numerateur est 0, le denominateur prend la valeur 1
	if (numerateur == 0) { denominateur = 1; return; }
	/* un denominateur ne devrait pas �tre 0;
	si c�est le cas, on sort de la m�thode */
	if (denominateur == 0) return;
	/* utilisation de l�algorithme d�Euclide pour trouver le Plus Grand Commun
	Denominateur (PGCD) entre le numerateur et le denominateur */
	int a = numerateur, b = denominateur;
	// on ne travaille qu�avec des valeurs positives...
	if (a < 0) a = -a; if (b < 0) b = -b;
	while (a != b) { if (a > b) a = a - b; else b = b - a; }
	// on divise le numerateur et le denominateur par le PGCD=a
	numerateur /= a; denominateur /= a;
	// si le denominateur est n�gatif, on fait passer le signe - au denominateur
	if (denominateur < 0) { denominateur = -denominateur; numerateur = -numerateur; }
}

const MATH::Fraction MATH::Fraction::somme(const MATH::Fraction& fadd) const
{
	int a, b, c, d;
	a = getNumerateur();
	b = getDenominateur();
	c = fadd.getNumerateur();
	d = fadd.getDenominateur();
	return MATH::Fraction(a * d + b * c, b * d);
}

const MATH::Fraction MATH::somme(const MATH::Fraction& fadd1, const MATH::Fraction& fadd2) 
{
	//definition d'une fonction pour faire fraction + fraction
	int a, b, c, d;
	a = fadd1.getNumerateur();
	b = fadd1.getDenominateur();
	c = fadd2.getNumerateur();
	d = fadd2.getDenominateur();
	return MATH::Fraction(a * d + b * c, b * d);
}

// //Les deux surcharges d'operator+ ci-dessous doivent etre definies ensemble pour pouvoir
// //faire fraction + fraction, int + fraction, ou fraction+int
// //MAIS NE PEUVENT PAS ETRE UTILISEES AVEC LA TROISIEME SURCHARGE
// //D'OPERATOR+ DEFINIE PLUS BAS, SINON AMBIGUITE POUR LE COMPILATEUR
//const MATH::Fraction MATH::Fraction::operator+(const MATH::Fraction& fadd) const
//{
// // surcharge de operator+ permettant de faire fraction + fraction ou fraction + int
//	int a, b, c, d;
//	a = getNumerateur();
//	b = getDenominateur();
//	c = fadd.getNumerateur();
//	d = fadd.getDenominateur();
//	return MATH::Fraction(a * d + b * c, b * d);
//
//}
//const MATH::Fraction MATH::operator+(const int i_add, const MATH::Fraction& fadd)
//{
// 
//	//surcharge de l'operator+ via une fonction pour faire int + fraction
//	int a = fadd.getNumerateur();
//	int b = fadd.getDenominateur();
//	return MATH::Fraction(i_add*b + a, b);
//}



// // La surcharge d'operator+ ci-dessous permet de faire 
// // fraction + fraction, fraction + int, int + fraction
// // MAIS NE PEUT PAS ETRE UTILISEE AVEC LES DEUX SURCHARGES
// // PRECEDENTES, AMBIGUITE POUR LE COMPILATEUR SINON
const MATH::Fraction MATH::operator+(const MATH::Fraction& f1add
	, const MATH::Fraction& fadd2)
{
	int a, b, c, d;
	a = f1add.getNumerateur();
	b = f1add.getDenominateur();
	c = fadd2.getNumerateur();
	d = fadd2.getDenominateur();
	return MATH::Fraction(a * d + b * c, b * d);

}



MATH::Fraction& MATH::Fraction::operator++()
{
	//definition de la surcharge operator++ en prefixe
	setFraction(1*getDenominateur() + getNumerateur(), getDenominateur());
	return *this;

}

MATH::Fraction MATH::Fraction::operator++(int)
{
	//definition de la surcharge incrementation postfixe
	Fraction old_fraction(getNumerateur(), getDenominateur());
	setFraction(1*getDenominateur() + getNumerateur(), getDenominateur());
	return old_fraction;
}

std::ostream& operator<<(std::ostream& t_flux, const MATH::Fraction& t_frac)
{
	//Surcharge de l'operator<< d'ecriture dans un flux de type ostream
	t_flux << t_frac.getNumerateur();
	if (t_frac.getDenominateur()!= 1)
		t_flux << "\\" << t_frac.getDenominateur()<<"\n";
	//On retourne le flux t_flux qui etait passe en parametre de la surcharge
	return t_flux;
}
