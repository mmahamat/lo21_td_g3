#ifndef FRACTION_H
#define FRACTION_H
#include <iostream>
#include <cstdio>

namespace MATH
{

	class Fraction
	{
	private:
	//Nous ne voulons pas rendre le numerateur et le denominateur visible
	//On les met en portee private, et on les rend accessible via des getter et setter
	//On cache les membres en utilisant le principe d'encapsulation
		int numerateur;
		int denominateur;
		void simplification();
	public:
		void setFraction(int n, int d);
		void setNumerateur(int n);
		void setDenominateur(int d);
		//methodes const pour pouvoir travailler avec des Fractions constantes, e.g. Nombre d'or
		const int getNumerateur() const { return numerateur; }; 
		const int getDenominateur() const { return denominateur; };

		//Fraction(int n=0, int d=1) { setFraction(n, d); }; //Un premier constructeur avec une affectation
		//Fraction(int n) { setFraction(n, 1); }; // Un autre constructeur qui fait une affectation
		//Fraction() {};
		Fraction(int n = 0, int d = 1) :numerateur(n), denominateur(d) // Un constructeur qui fait une initialisation des membres
		{
			if (d == 0)
			{
				std::cout << "denominateur ne peut pas etre nul!\n";
				denominateur = 1;

			}
			std::cout << "construction a l'adresse: " << this << "\n";
			simplification();
		};

		~Fraction(){std::cout <<"destruction a l'adresse:"<< this << "\n";};

		//Fraction(int d) { setFraction(0, d); };

		const Fraction somme(const Fraction& fadd) const; //une methode pour additionner deux fractions
		//const Fraction operator+(const Fraction& fadd) const; //premiere surcharge pour faire fraction + fraction ou fraction+int
		
		Fraction& operator++(); //surcharge pour operator++ en incrementation prefixe
		Fraction operator++(int); //surcharge pour operator++ en incrementation postfixe, (int) est un parametre fictif qui sert
		// a differencier entre le prefixe et le postfixe
	};
	//END CLASS FRACTION

	const Fraction somme(const Fraction& fadd1, const Fraction& fadd2); //une fonction pour additionner deux fractions

	//const Fraction operator+(const int i_add, const MATH::Fraction& fadd); //surcharge operator+ via une fonction
	//pour pouvoir faire int + frac; a utiliser conjointement avec la methode qui surcharge operator+ dans la classe Fraction
	//Ne peut pas etre utilisee avec la surcharge qui suit!!!!!

	
	const Fraction operator+(const Fraction& f1add, const Fraction& fadd2); //surcharge pour pouvoir
	// additioner deux fractions, ou int avec fraction (int + frac ou frac + int)

	class FractionException
	{
		//classe pour gerer les exceptions liees a notre classe Fraction
	private:
		char m_info[256];
	public:
		FractionException(const char* t_info)
		{
			strcpy_s(m_info, t_info); 
			//strcpy peut ne pas marcher selon le degre de surete de votre compilateur 
			// (mon cas, il considere strcpy comme unsafe)
			//il faut faire un #include <cstdio> pour utiliser strcpy_s qui est considere 
			//comme safe
		}
		const char* getInfo() const { return m_info; }; //retour code d'erreur
	};
}//FIN NAMESPACE MATH


//surcharge de l'operateur d'ecriture
//dans un flux de type ostream, notez qu'il faut declarer et definir la surcharge de fa�on globale
std::ostream& operator<<(std::ostream& t_flux, const MATH::Fraction& t_frac); 


#endif

