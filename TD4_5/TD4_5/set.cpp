#include "set.h"

namespace Set {
	
	std::initializer_list<Couleur> Couleurs = { Couleur::rouge, Couleur::mauve, Couleur::vert };
	std::initializer_list<Nombre> Nombres = { Nombre::un, Nombre::deux, Nombre::trois };
	std::initializer_list<Forme> Formes = { Forme::ovale, Forme::vague, Forme::losange };
	std::initializer_list<Remplissage> Remplissages = { Remplissage::plein, Remplissage::vide, Remplissage::hachure };

	string toString(Couleur c) {
		switch (c) { 
		case Couleur::rouge: return "R";
		case Couleur::mauve: return "M";
		case Couleur::vert: return "V";
		default: throw SetException("Couleur inconnue");
		}
	}

	string toString(Nombre v) {
		switch (v) {
		case Nombre::un: return "1";
		case Nombre::deux: return "2";
		case Nombre::trois: return "3";
		default: throw SetException("Nombre inconnue");
		}
	}

	string toString(Forme f) {
		switch (f) {
		case Forme::ovale: return "O";
		case Forme::vague: return "~";
		case Forme::losange: return "\004";
		default: throw SetException("Forme inconnue");
		}
	}

	string toString(Remplissage r) {
		switch (r) {
		case Remplissage::plein: return "P";
		case Remplissage::vide: return "_";
		case Remplissage::hachure: return "H";
		default: throw SetException("Remplissage inconnu");
		}
	}

	std::ostream& operator<<(std::ostream& f, Couleur c) { f << toString(c); return f; }
	std::ostream& operator<<(std::ostream& f, Nombre v) {	f << toString(v); return f; }
	std::ostream& operator<<(std::ostream& f, Forme x) { f << toString(x);  return f; }
	std::ostream& operator<<(std::ostream& f, Remplissage r) { f << toString(r); return f; }

	void printCouleurs(std::ostream& f) {
		for (auto c : Couleurs) f << c << " ";
		f << "\n";
	}

	void printNombres(std::ostream& f) {
		for (auto v : Nombres) f << v << " ";
		f << "\n";
	}

	void printFormes(std::ostream& f) {
		for (auto x : Formes) f << x << " ";
		f << "\n";
	}

	void printRemplissages(std::ostream& f) {
		for (auto r : Remplissages) f << r << " ";
		f << "\n";
	}
	//fonctions/methodes liees a class CARTE
	std::ostream& operator<<(std::ostream& t_flux, const Carte& t_c)
	{
		t_flux << "("<<t_c.getNombre()
			<<", "<<t_c.getCouleur()
			<<", "<<t_c.getForme()
			<<", "<<t_c.getRemplissage()<<")";
		return t_flux;
	}

	//fonctions/methodes liees a class Jeu
	//constructeur un peu gros donc on le definit dans set.cpp
	//mais vous pouvez le definir dans set.h
	Jeu::Jeu()
	{
		size_t t_i = 0;
		//on utilise l'ecriture for(auto my_var : type) pour iterer
		//sur toutes les valeurs du type, ici nos enumerations
		//Similaire au for avec range() de python
		for (Couleur t_c : Couleurs)
			for (Nombre t_n : Nombres)
				for (Forme t_f : Formes)
					for (Remplissage t_r : Remplissages)
						cartes[t_i++] = new Carte(t_c, t_n, t_f, t_r);
	};

	//fonctions/methodes liees a class Pioche
	const Carte& Pioche::piocher()
	{
		if (nb == 0)
			throw SetException("La pioche est vide ...");
		//Ne pas oublier que votre pioche doit etre aleatoire, sinon cela serait trop facile ...
		size_t rand_i = rand() % nb; //rand() renvoie un int compris entre 0 et nb -1
		const Carte* t_c = cartes[rand_i]; //la carte qui sera renvoyee
		//Quand on pioche une carte, elle sort de la pioche; les cartes qui suivent avancent donc
		for (size_t t_pos = rand_i + 1; t_pos < nb; t_pos++)
			cartes[t_pos - 1] = cartes[t_pos];
		//la taille de la pioche diminue
		nb--;
		//On retourne la carte qu'on avait piochee
		return *t_c;

	}

	//fonctions/methodes liees a class Plateau
	//copy ctor
	Plateau::Plateau(const Plateau& p) :cartes(new const Carte* [p.nb]), nb(p.nb),
		nbMax(p.nb) {
		for (size_t i = 0; i < nb; i++) cartes[i] = p.cartes[i];
	}

	void Plateau::ajouter(const Carte& c)
	{
		//Gestion de si le plateau est trop grand:
		// -creer un tableau de pointeurs de cartes beaucoup plus grand
		// -assigner les cartes existantes de cartes[] dans nouvelles_cartes[]
		// -supprimer l'espace alloue pour l'ancien tableau
		if (nb == nbMax)
		{
			const Carte** nouvelles_cartes = new const Carte * [2 * nbMax + 1];
			for (size_t t_pos = 0; t_pos < nb; t_pos++)
				nouvelles_cartes[t_pos] = cartes[t_pos];
			auto old_cartes = cartes;
			cartes = nouvelles_cartes;
			nbMax = 2 * nbMax + 1;
			delete[] old_cartes;
		}

		//ajout de la carte
		cartes[nb++] = &c;  //On met la carte c a la pos nb, et on incremente nb
	}

	void Plateau::retirer(const Carte& c) 
	{
		//retirer une carte: il faut verifier que celle-ci existe
		size_t t_pos = 0;

		while (t_pos < nb && cartes[t_pos] != &c) 
			t_pos++;

		if (t_pos == nb) //gerer le fait qu'on a tout parcouru, et que la carte n'est pas dispo
			throw SetException("Carte inexistante");

		t_pos++;

		while (t_pos < nb) 
		{
			cartes[t_pos - 1] = cartes[t_pos]; 
			t_pos++;
		}
		nb--; //une carte est enlevee du plateau de jeu
	}

	void Plateau::print(ostream& t_flux) const 
	{
		for (size_t t_pos = 0; t_pos < nb; t_pos++) {
			if (t_pos % 4 == 0) 
				t_flux << "\n";
			t_flux << *cartes[t_pos] << " ";
		}
		t_flux << "\n";
	}

	Plateau& Plateau::operator=(const Plateau& p) //operateur d'affectation
		//pour un potentiel memento
	{
		if (this != &p)
		{
			if (p.nb > nbMax)
			{
				const Carte** newtab = new const Carte * [p.nb];
				for (size_t i = 0; i < nb; i++) newtab[i] = p.cartes[i];
				auto old = cartes;
				cartes = newtab;
				nb = nbMax = p.nb;
				delete[] old;
			}
			else 
			{
				for (size_t i = 0; i < nb; i++)
					cartes[i] = p.cartes[i];
				nb = p.nb;
			}
		}
		return *this;
	}

	ostream& operator<<(ostream& t_flux, const Plateau& t_plateau)
	{
		//appel de la methode print() du plateau t_plateau
		t_plateau.print(t_flux);
		return t_flux;
	}

	bool Combinaison::estUnSET() const 
	{
		//rien de bien complique, mais long a ecrire, correspond a l'enonce
		bool c = (c1->getCouleur() == c2->getCouleur() && c1->getCouleur() == c3->getCouleur()) 
			||	(c1->getCouleur() != c2->getCouleur()
				&& c1->getCouleur() != c3->getCouleur() 
				&& c2->getCouleur() != c3->getCouleur());
		bool n = (c1->getNombre() == c2->getNombre()&& c1->getNombre() == c3->getNombre()) 
			|| (c1->getNombre() != c2->getNombre() 
				&& c1->getNombre() != c3->getNombre() 
				&& c2->getNombre() != c3->getNombre());
		bool f = (c1->getForme() == c2->getForme() && c1->getForme() == c3->getForme())
			|| (c1->getForme() != c2->getForme()
				&& c1->getForme() != c3->getForme() 
				&& c2->getForme() != c3->getForme());
		bool r = (c1->getRemplissage() == c2->getRemplissage() && c1->getRemplissage() == c3->getRemplissage())
			|| (c1->getRemplissage() != c2->getRemplissage() 
				&& c1->getRemplissage() != c3->getRemplissage() 
				&& c2->getRemplissage() != c3->getRemplissage());
		return c && n && f && r;
	}

	ostream& operator<<(ostream& t_flux, const Combinaison& t_comb) {
		t_flux << "[ " << t_comb.getCarte1() 
			   << " ; " << t_comb.getCarte2() 
			   << " ; " << t_comb.getCarte3() << " ]";
		return t_flux;
	}
	//
}