#include "set.h"
using namespace Set;

int main() {
	//try {

	//	printCouleurs();
	//	printNombres();
	//	printFormes();
	//	printRemplissages();

	//	Carte ma_carte(Couleur::rouge, Nombre::un,Forme::losange,Remplissage::hachure);
	//	std::cout << ma_carte << "\n";
	//}
	//catch (SetException& e) {
	//	std::cout << e.getInfo() << "\n";
	//}
	//
	//system("pause");
	//return 0;
	try {
		Controleur c;
		c.distribuer();
		cout << c.getPlateau();
		c.distribuer();
		cout << c.getPlateau();
	}
	catch (SetException& e) {
		std::cout << e.getInfo() << "\n";
	}
	return 0;
}