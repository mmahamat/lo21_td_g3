#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
using namespace std;

namespace Set {
	// classe pour gérer les exceptions dans le set
	class SetException {
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caractéristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un=1, deux=2, trois=3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caractéristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// écriture d'une caractéristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caractéristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caractéristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);

	class Carte {
	private:
		Couleur couleur;
		Nombre nombre;
		Forme forme;
		Remplissage remplissage;
	public:
		Carte(Couleur c, Nombre v, Forme f, Remplissage r) :couleur(c), nombre(v),
			forme(f), remplissage(r) {}
		Couleur getCouleur() const { return couleur; }
		Nombre getNombre() const { return nombre; }
		Forme getForme() const { return forme; }
		Remplissage getRemplissage() const { return remplissage; }
		~Carte() = default; // Pas necessaire car nous ne gerons pas de pointeur, que des attributs simples
		Carte(const Carte& c) = default; // default permet d'utiliser le ctor par recopie genere par le compilateur
		//De plus, optionel car attributs 'primitifs' (des enumerations simples)
		Carte& operator=(const Carte& c) = default; // Idem
	
	};
	//affichage sur un flux ostream de cartes
	std::ostream& operator<<(std::ostream& t_flux, const Carte& t_c);

	//classe de jeu
	class Jeu
	{
	private:
		const Carte* cartes[81];
	public:
		Jeu();
		~Jeu() {for (size_t i = 0; i < getNbCartes(); i++) delete cartes[i];};
		size_t getNbCartes() const{ return 81; };
		//Ce qu'on interdit d'utiliser: copy ctor, operator=
		Jeu(const Jeu& t_j) = delete;
		Jeu& operator=(const Jeu& t_j) = delete;

		const Carte& getCarte(size_t i) const { /*if (i >= 81) throw SetException("invalid index"); return *cartes[i];*/
			return i >= 81 ? throw SetException("invalid index") : *cartes[i]; //operateur ternaire
		};

	};

	//classe de pioche
	class Pioche
	{
	private:
		size_t nb = 0; //pas de cartes quand la pioche est initialement creee.
		const Carte** cartes=nullptr; //idem
	public:
		explicit Pioche(const Jeu& t_j) :cartes(new const Carte*[t_j.getNbCartes()]), nb(t_j.getNbCartes())
		{
			for (size_t t_i = 0; t_i < nb; t_i++) cartes[t_i] = &t_j.getCarte(t_i);
		};
		~Pioche() { delete[] cartes; };
		size_t getNbCartes() const { return nb; };
		bool estVide() { return nb == 0 ? true : false; };
		const Carte& piocher();

		//operations interdites
		Pioche(const Pioche& t_p) = delete;
		Pioche& operator=(const Pioche& t_p) = delete;
	};

	//classe du plateau 
	class Plateau {
	private:
		const Carte** cartes = nullptr;
		size_t nbMax = 0; //taille reelle du tableau cartes
		size_t nb = 0; //nb cartes du tableau
	public:
		Plateau() = default;
		~Plateau() { delete[] cartes; }
		size_t getNbCartes() const { return nb; }
		void ajouter(const Carte& c);
		void retirer(const Carte& c);
		void print(ostream& t_flux = cout) const;
		Plateau(const Plateau& p);
		Plateau& operator=(const Plateau& p);
	
	};
	//affichage d'un plateau sur un flux ostream
	ostream& operator<<(ostream& t_flux, const Plateau& t_plateau);

	//class Combinaison
	class Combinaison {
	private:
		const Carte* c1;
		const Carte* c2;
		const Carte* c3;
	public:
		Combinaison(const Carte& t_c1, const Carte& t_c2, const Carte& t_c3) :c1(&t_c1), c2(&t_c2),
			c3(&t_c3) {}
		bool estUnSET() const;
		const Carte& getCarte1() const { return *c1; }
		const Carte& getCarte2() const { return *c2; }
		const Carte& getCarte3() const { return *c3; }
		//Non obligatoire de definir des operateurs d'affectation, ctor copy ou dtor car types 'simples'
		//Utilisation des operateurs/methodes generees automatiquement par le compilateur
		~Combinaison() = default;
		Combinaison(const Combinaison& c) = default;
		Combinaison& operator=(const Combinaison& c) = default;

	};
	ostream& operator<<(ostream& t_flux, const Combinaison& t_comb);

	//classe Controleur, cerveau de l'application
	class Controleur
	{
		//les attributs
	private:
		Jeu jeu;
		Plateau plateau;
		Pioche* pioche=nullptr;
	public:
		//les methodes
		Controleur() { pioche = new Pioche(jeu); };
		~Controleur() { delete pioche; };
		void distribuer() {};
		const Plateau& getPlateau() const { return plateau; };
	};
}


#endif