#include "container.h"
#include "myvector.h"
#include "mystack.h" 
#include <iostream>
#include <vector>

struct exemple_sup //pour objet fonction, ou on a surcharge operator()
	//class exemple_sup marche aussi
	//operator() sera dans la partie publique de la classe
{
	bool operator()(int a, int b) { return a > b; }; //notre operateur() surcharg�
};

int main()
{
	//test des classes vues en TD
	TD::Vector<int> Mon_vecteur;
	Mon_vecteur.push_back(1);
	Mon_vecteur.push_back(12);
	Mon_vecteur.push_back(42);
	Mon_vecteur.push_back(50);
	for (size_t i = 0; i < Mon_vecteur.size(); i++)
		std::cout << Mon_vecteur[i] << "\n";
	Mon_vecteur.pop_back();
	std::cout << "Apres pop_back():\n";
	for (size_t i = 0; i < Mon_vecteur.size(); i++)
		std::cout << Mon_vecteur[i] << "\n";

	//test de la Stack avec l'adapteur de classe
	StackClass<int> ma_stack;
	ma_stack.push(1);
	ma_stack.push(504);
	ma_stack.push(2599);
	std::cout << "affichage stack adapteur de classe\n";
	while(!ma_stack.empty())
	{
		std::cout << ma_stack.top() << "\n";
		ma_stack.pop();
	}
	//test de la stack avec l'adapteur d'objet
	//StackObject<int> ma_stack_object; //avec notre code
	StackObject<int, std::vector<int>> ma_stack_object; //avec un std::vector<int>
	ma_stack_object.push(42);
	ma_stack_object.push(999);
	ma_stack_object.push(1200);
	ma_stack_object.push(7);
	//std::cout << "affichage stack object\n";
	//while (!ma_stack_object.empty())
	//{
	//	std::cout << ma_stack_object.top() << "\n";
	//	ma_stack_object.pop();
	////
	std::cout << "essai minimum_element sans rien du tout:\n";
	try
	{
		auto it_value = TD::minimum_element(ma_stack_object.begin(), ma_stack_object.end());
		std::cout << *it_value << " trouve!\n";
	}
	catch (const TD::ContainerException& co_err)
	{
		std::cout << co_err.what() << "\n";
	}
	std::cout << "essai minimum_element avec une fonction lambda (element le plus grand):\n";
	try //version avec une fonction lambda:
	{
		/*
		Le principe des fonctions lambda de C++ (C++ 11 & later)
		est le m�me que celles pr�sentes en Python ou Lisp
		*/
		auto it_value = TD::minimum_element(ma_stack_object.begin(),
			ma_stack_object.end(),
			[](int a, int b)
		{return a > b; });
		std::cout << *it_value << " avec une fonction lambda: trouve!\n";
	}
	catch (const TD::ContainerException& co_err)
	{
		std::cout << co_err.what() << "\n";
	}

	std::cout << "essai minimum_element avec un objet fonction:\n";
	try //version avec une fonction lambda:
	{
		auto it_value = TD::minimum_element(ma_stack_object.begin(),
			ma_stack_object.end(),
			exemple_sup());
		std::cout << *it_value << " avec un objet fonction: trouve!\n";
	}
	catch (const TD::ContainerException& co_err)
	{
		std::cout << co_err.what() << "\n";
	}
	return 0;
}