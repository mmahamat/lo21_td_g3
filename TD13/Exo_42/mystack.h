#ifndef MY_STACK
#define MY_STACK
#include "container.h"
#include "myvector.h"

template<class T, class THE_CONTAIN = TD::Vector<T>>
//THE_CONTAIN va nous permettre de mettre en place le design pattern strategy 
//dans les methodes des templates de classe plus bas,
//en supposant que THE_CONTAIN est un conteneur SEQUENTIEL
class StackObject
{//adapteur d'objet pour la Stack
private:
	THE_CONTAIN m_conteneur;
public:
	StackObject() : m_conteneur(0) {}
	void push(const T& x) { m_conteneur.push_back(x); }; //avec strategy via les patrons de classe, on pourra choisir
	// de fa�on dynamique, en fonction du conteneur SEQUENTIEL passe en parametre de template,
	//la bonne methode push
	void pop() { m_conteneur.pop_back(); };
	size_t size() const {
		return m_conteneur.size();
	};
	T& top() { return m_conteneur.back(); }
	const T& top()const { return m_conteneur.back(); };
	void clear() {
		m_conteneur.clear();
	}
	bool empty()const { return m_conteneur.empty(); };
	//adapteur de classe pour faire un iterateur 
	class iterator : public THE_CONTAIN::iterator
	{
	public:
		iterator() : THE_CONTAIN::iterator() {}
		iterator(typename THE_CONTAIN::iterator ite) :THE_CONTAIN::iterator(ite) {}
	};
	iterator begin() { return iterator(m_conteneur.begin()); }
	iterator end() { return iterator(m_conteneur.end()); }
	//adapteur de classe �galement
	class const_iterator : public THE_CONTAIN::const_iterator
	{
	public:
		const_iterator() : THE_CONTAIN::const_iterator() {}
		const_iterator(typename THE_CONTAIN::const_iterator ite) :THE_CONTAIN::const_iterator(ite) {}
	};
	const_iterator begin() const{ return const_iterator(m_conteneur.begin()); }
	const_iterator end() const { return const_iterator(m_conteneur.end()); }
};

template<class T, class THE_CONTAIN = TD::Vector<T>>
class StackClass : private THE_CONTAIN
{ //version avec un adapteur de classe
public:
	StackClass() : THE_CONTAIN(0) {};
	void push(const T& x) /*{ THE_CONTAIN::push_back(x); };*/
	{
		this->push_back(x); //possibilite d'utiliser this->methode()
	}
	void pop() { THE_CONTAIN::pop_back(); };
	/*size_t size() const {
		return THE_CONTAIN::size();
	};*/
	using THE_CONTAIN::size; //possibilite d'utiliser la directive using
	T& top() { return THE_CONTAIN::back(); }
	const T& top()const { return THE_CONTAIN::back(); };

	void clear() {
		THE_CONTAIN::clear();
	}
	bool empty() const { return THE_CONTAIN::empty(); };
	//directives using, plus rapides a mettre en place qu'une classe
	//vu qu'on a mis en place les design patterns strategy, adapter en place
	//il est tout a fait possible de reutiliser les iterateurs du container
	//THE_CONTAIN
	using typename THE_CONTAIN::iterator;
	using typename THE_CONTAIN::const_iterator;
	using THE_CONTAIN::begin;
	using THE_CONTAIN::end;
};

#endif