#if !defined(_GRAPH_H)
#define _GRAPH_H
#include<string>
#include<stdexcept>
using namespace std;
class GraphException : public exception {
	string info;
public:
	GraphException(const string& i) noexcept :info(i) {}
	virtual ~GraphException() noexcept {}
	const char* what() const noexcept { return info.c_str(); }
};

#include<list>
#include<vector>
#include<iostream>
#include<string>

class Graph {
	vector<list<unsigned int> > adj;
	string name;
	//unsigned int nb_edges;
	size_t nb_edges; //plus facile pour traquer le nb d'aretes
public:
	Graph(const string& n, size_t nb) :name(n), nb_edges(0), adj(nb) {};
	/*on cree nb listes doubles chainees vides avec adj(nb) */
	const string& getName() const {
		return name;
	};
	size_t getNbVertices() const { return adj.size(); };
	size_t getNbEdges() const { return nb_edges; };
	void addEdge(unsigned int i, unsigned int j);
	void removeEdge(unsigned int i, unsigned int j);
	//size_t calculNbEdges() const; //possible d'avoir une methode
	//pour calculer "continuellement" le nombre d'aretes
	const list<unsigned int>& getSuccessors(unsigned int i) const;
	const list<unsigned int> getPredecessors(unsigned int i) const;
};
ostream& operator<<(ostream& f, const Graph& G);

//classe de graphe generale:

#include<map>
#include<set>
#include<string>
#include<iostream>
using namespace std;
template<class Vertex>
class GraphG {
	map<Vertex, set<Vertex> > adj;
	string name;
	size_t nb_edges; //plus facile pour traquer le nb d'aretes
public:
	GraphG(const string& n) :nb_edges(0), name(n) {};
	const string& getName() const {
		return name;
	};
	size_t getNbVertices() const { return adj.size(); };
	size_t getNbEdges() const { return nb_edges; };
	void addVertex(const Vertex& i);
	void addEdge(const Vertex& i, const Vertex& j);
	void removeEdge(const Vertex& i, const Vertex& j);
	void removeVertex(const Vertex& i);
	void print(ostream& f) const;
	
	//iterateurs question 2 optionnelle
	class vertex_iterator : public map<Vertex, set<Vertex>>::const_iterator
	{
		//pour pouvoir iterer sur nos sommets
	public:
		vertex_iterator() :map<Vertex, set<Vertex>>::const_iterator() {}
		vertex_iterator(typename map<Vertex, set<Vertex>>::const_iterator t_it) :map<Vertex, set<Vertex>>::const_iterator(t_it)
		{}
		const Vertex& operator*() const {
			return map<Vertex, set<Vertex>>::const_iterator::operator*().first;/*first renvoie la clef*/
		};

	};
	vertex_iterator begin_vertex() const { return vertex_iterator(adj.begin()); };
	vertex_iterator end_vertex() const { return vertex_iterator(adj.end()); };

	class successor_iterator : public set<Vertex>::const_iterator //iterer sur les successeurs
	{
		//but de la classe : r�cup�rer les successeurs du noeud x
	public:
		successor_iterator() :set<Vertex>::const_iterator() {};
		successor_iterator(typename set<Vertex>::const_iterator t_it) :set<Vertex>::const_iterator(t_it) {};
	};

	successor_iterator begin_successor(const Vertex& x) const
	{ //d�but de la liste des successeurs
		typename map<Vertex, set<Vertex>>::const_iterator it = adj.find(x); //on essaie de trouver la liste des successeurs de x
		if (it != adj.end())
			return successor_iterator(it->second.begin());
		else throw GraphException("could not retrieve successors of x");
	};
	successor_iterator end_successor(const Vertex& x) const
	{//fin de la liste des successeurs
		typename map<Vertex, set<Vertex>>::const_iterator it = adj.find(x); //on essaie de trouver la liste des successeurs de x
		if (it != adj.end())
			return successor_iterator(it->second.end());
		else throw GraphException("could not retrieve successors of x, x does not exist");
	};
};



template<class Vertex>
void GraphG<Vertex>::addVertex(const Vertex& i) {
	//adj[i];
	adj.insert(make_pair(i, set<Vertex>())); //deuxieme fa�on de faire
}

template<class Vertex>
void GraphG<Vertex>::addEdge(const Vertex& i, const Vertex& j)
{
	if (adj[i].insert(j).second) //
	{
		nb_edges++;
	}
	adj[j]; //on cr�e l'entr�e pour j si le sommet n'existait pas auparavant
}

template<class Vertex>
void  GraphG<Vertex>::removeEdge(const Vertex& i, const Vertex& j) 
{
	typename map<Vertex, set<Vertex> >::iterator the_pair = adj.find(i);
	if (the_pair != adj.end())
	{
		nb_edges -= the_pair->second.erase(j);
		/*Equivalent a:
		* if(the_pair->second.erase(j))
		*	nb_edges--;
		*/
	}
}
template<class Vertex>
void  GraphG<Vertex>::removeVertex(const Vertex& i)
{
	typename map<Vertex, set<Vertex> >::iterator to_delete = adj.end();
	for (auto it = adj.begin(); it != adj.end(); ++it)
	{
		if(it->first != i)
			nb_edges -= it->second.erase(i);
		else
		{
			nb_edges -= it->second.size();
			to_delete = it;
		}

	}
	if(to_delete != adj.end())
	{
		adj.erase(to_delete);
	}
}
template<class V> ostream& operator<<(ostream& f, const GraphG<V>& G)
{
	G.print(f);
	return f;
}
template<class V>void GraphG<V>::print(ostream& f) const
{
	f << "Name: " << getName() << " has " << getNbEdges() << " edge(s)\n";
	f << " with a nb of vertices equal to: " << getNbVertices() << "\n";
	f << "------\nvertices and their adjacency list: " << "\n";
	for (auto it = adj.begin(); it != adj.end(); ++it)
	{
		f << it->first << ": "; //ieme sommet
		for (auto it2 = it->second.begin();
			it2 != it->second.end(); ++it2)
		{
			f << *it2 << ", "; //affichage des successeurs du sommet i
		}
		f << "\n";
	}
}
#endif
