#include "graphe.h"

int main()
{
	try {
		Graph G1("G1", 5); cout << G1;
		G1.addEdge(0, 1); G1.addEdge(0, 2); G1.addEdge(1, 2); G1.addEdge(1, 3);
		G1.addEdge(1, 4); G1.addEdge(3, 0);
		cout << G1;

		GraphG<string> Prereqs("Prerequesites");
		Prereqs.addEdge("MT90", "MT91"); Prereqs.addEdge("MT90", "MT23"); Prereqs.addEdge("MT91", "MT22"); 
		//la creation des aretes va nous creer les sommets qui n'existent pas encore
		Prereqs.addEdge("MT90", "NF93"); Prereqs.addEdge("MT91", "SY01"); Prereqs.addEdge("MT23", "MT09");
		Prereqs.addEdge("SY01","SY02"); Prereqs.addEdge("SY02", "SY09"); Prereqs.addEdge("SY09", "SY19");
		Prereqs.addEdge("NF93", "NF16"); Prereqs.addEdge("NF92", "NF18"); Prereqs.addEdge("NF18", "NF26");
		Prereqs.addEdge("INF1", "NF92"); Prereqs.addEdge("NF93", "IA01"); Prereqs.addEdge("INF1", "IA01");
		cout << Prereqs;
	}
	catch (exception e) { std::cout << e.what() << "\n"; }
	return 0;
}