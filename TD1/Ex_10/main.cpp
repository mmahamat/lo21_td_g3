#include <iostream>

/*
* Exercice 10
* Manipulation des pointeurs, pointeurs const et pointeurs constants
* Les pointeurs constants ne peuvent pointer que sur la m�me adresse m�moire
* ==> On ne peut pas pointer sur une autre variable, mais on peut modifier la valeur de la variable
* Les pointeurs const pointent sur des variables que le pointeur consid�re comme constantes
* ==> Il ne sera pas possible de modifier la valeur de la variable point�e par ces pointeurs
* Les pointeurs constants sur const ne peuvent pas changer la valeur de la variable point�e 
* et ne peuvent pas pointer d'autres variables!
*/

int main()
{
  
    double* pt0 = 0; //OK mais pr�f�rence ici pour nullptr
    pt0 = nullptr;

    //double* pt1 = 4096; // Pas bon, 4096 est une adresse ici, on ne sait pas ce qu'il y a ici ...
    double* pt1 = nullptr;
    double* pt2 = (double*)4096; // CONVERSION OK mais attention, dangereux car c'est une adresse, c'est quoi le contenu pr�sent ici? ...
    std::cout << "\n";

    void* pt3 = pt1; // Si pt1 est d�sign� comme �tant une erreur, alors cette ligne ne compilera pas ... Mais conversion accept�e car conversion implicite de double* vers void* accept�e

   // pt1 = pt3; // PAS BON, pointeur sur double ne peut pas recevoir implicitement un pointeur sur void, il faut le faire de fa�on explicite !!!
    pt1 = (double*)pt3; // OK car conversion explicite de type C
    pt1 = static_cast<double*>(pt3); // alternative possible en utilisant l'op�rateur static_cast (C++)



    double d1 = 36;
    const double d2 = 36;

    double* pt4 = &d1;
    const double* pt5 = &d1; //OK, mais voyez la suite !

    *pt4 = 2.1; //OK, on change la valeur point�e par pt4
   // *pt5 = 2.1; //PAS OK !!!! On ne peut pas modifier d1 via pt5;

   // pt4 = &d2; //PAS OK, on ne peut pas faire pointer un pointeur sur une variable constante
    pt5 = &d2; //OK, les deux sont du m�me type.

    double* const pt6 = &d1; //OK, pointeur constant, il pointera toujours sur d1!
   // pt6 = &d1; //On ne peut plus changer la variable point�e par pt6! C'est un pointeur  CONSTANT
    *pt6 = 2.78; //OK, on peut changer la valeur de la variable point�e par pt6 car pt6 est un pointeur constant mais pas un pointeur sur const

   // double* const pt6b = &d2; // LES TYPES NE CORRESPONDENT PAS!
    const double* const pt7 = &d1; //oof, c'est une d�finition tr�s stricte de pt7, OK mais on ne pourra pas faire grand chose avec ...
   // pt7 = &d1; //pas bon, on ne peut plus changer la variable qui est point�e par pt7.
   // *pt7 = 2.78; //Pas BON, on ne peut pas changer la valeur point�e car [const double*]!!!

    d1 = 5.0;

    double const* pt8 = &d1; // It's ok, mais attention, c'est un pointeur qui pointe une valeur constante
    pt8 = &d2; // Okay too
    pt8 = &d1; //ok
    //*pt8 = 3.14; //Non, on ne peut pas changer la valeur point�e car elle est d�clar�e comme �tant un pointeur constant.

    return 0;
}

