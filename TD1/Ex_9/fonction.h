/*
* Les fonctions inline sont plac�es et d�finies dans le fichier d'en-t�te.
* Elles sont remarquables par le mot cl� "inline" plac� devant leur d�finition.
* Les fonctions inlines sont g�n�ralement des fonctions qui effectuent des petits calculs
* ou op�rations (on retournera sur ce point lors de la d�couverte des classes)
* Chaque appel � la fonction inline dans les .cpp sera remplac� par le corps de la fonction d�fini dans le 
* fichier d'en-t�te.
* D'o� la n�cessit� d'avoir des fonctions courtes et r�alisant des t�ches simples.
* Cependant, il ne faut pas en abuser, cela peut rendre le programme tr�s lent
*/

#ifndef EXO_SURCHARGE
#define EXO_SURCHARGE

inline int fct(int x) { std::cout << "1:" << x << "\n"; return 0; }
inline int fct(float y) { std::cout << "2:" << y << "\n"; return 0; }
inline int fct(int x, float y) { std::cout << "3:" << x << y << "\n"; return 0; }
inline float fct(float x, int y) { std::cout << "4:" << x << y << "\n"; return 3.14; }


void exercice_surcharge();


#endif // !EXO_SURCHARGE
