#include <iostream>
#include "fonction.h"

/*
* Deux buts dans cet exercice:
*	manier la surcharge de fonctions (fonctions avec le m�me nom,
*	r�alisant un traitement similaire (ou non, mais ...) 
*	mais qui prennent en entr�e des arguments diff�rents
*	
*	Passer de fonctions d�finies dans le fichier source .cpp � des fonctions 
*	inline d�finies dans le fichier d'en-t�te .h
*/

//
// //4 surcharges de fct([...]) sont consid�r�es ici:
//int fct(int x) { std::cout << "1:" << x << "\n"; return 0; }
//int fct(float y) { std::cout << "2:" << y << "\n"; return 0; }
//int fct(int x, float y) { std::cout << "3:" << x << y << "\n"; return 0; }
//float fct(float x, int y) { std::cout << "4:" << x << y << "\n"; return 3.14; }


void exercice_surcharge() {
	int i = 3, j = 15;
	float x = 3.14159, y = 1.414;
	char c = 'A';
	double z = 3.14159265;
	fct(i); //appel 1 qui est connu du compilateur
	fct(x); //appel 2 qui est connu du compilateur
	fct(i, y); //appel 3 qui est connu
	fct(x, j); //appel 4 qui est connu
	fct(c); //appel 5 qui est connu, il y a une conversion implicite de char vers int
	
	//fct(i, j); //appel 6 ambigu car cela peut �tre fct(int x, float y) ou fct(float x, int y) 
	/*
	* Diff�rentes fa�ons existent pour changer le type d'une variable
	* Op�ration d�nomm�e casting
	*
	*/
	fct((int)c); // premi�re fa�on en version C pour caster, correspond � fct(int c) de l'appel 5
	fct(static_cast<int>(c)); //cast en version C++ de l'appel 5
	fct((float)i, j); // correspond � fct(float x, int y)
	fct(static_cast<float>(i), j); // alternative C++
	fct(static_cast<float>(i), c); //appel 7
	fct(i, z); //appel 8 ok, conversion implicite de double vers float
	fct(i, static_cast<float>(z)); //appel 8 en faisant une conversion explicite
	fct(z, (int)z); //appel 9 avec un cast explicite de type C.
}

