#include "fonction.h"

/*
* Exercice 8
* Manier les espaces de nom et l'op�rateur de port�e "MonNamespace::"
*/


//using namespace Anglais;
//using namespace Mandarin; 

//Si on fait un using namespace des deux namespaces, il y aura ambiguit� pour le compilateur!
//En effet, il ne saura pas quelle fonction bonjour() 
//correspond � quelle d�finition de fonction dans fonction.cpp
//car les fonctions pr�sentes dans chaque namespace sont visibles de fa�on globale.
//L'op�rateur de port�e Anglais:: (ou Mandarin::) permet d'acc�der 
//� chaque fonction bonjour() de leurs namespaces respectifs.

int main()
{
	//bonjour();
	//bonjour();
	Anglais::bonjour();
	Mandarin::bonjour();
	return 0;
}