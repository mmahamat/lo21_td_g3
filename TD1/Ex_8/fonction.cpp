#include "fonction.h"
#include <iostream>

//using namespace std;
/*
* using namespace std nous permet d'ecrire cout << et cin >>
* au lieu de std::cout et std::cin
*/

//void Mandarin::bonjour()
//{
//	std::cout << "nichao\n";
//
//}
//
//void Anglais::bonjour()
//{
//	std::cout << "hello\n";
//
//}

/*
* Il est aussi possible de d�finir les fonctions de la m�me fa�on qu'on a d�fini
* les namespaces dans fonction.h.
* Les deux fa�ons de faire sont similaires, 
* c'est donc � vous de choisir comment vous voulez �crire tout �a.
* (Cela peut aussi d�pendre des conventions de programmation de votre future entreprise)
*/
namespace Mandarin
{
	void bonjour()
	{
		std::cout << "nichao\n";
	}
}

namespace Anglais
{
	void bonjour()
	{
		std::cout << "hello\n";
	}
}
