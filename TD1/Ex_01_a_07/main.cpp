/*
* Exercices 1,2,3,4,5,6,7
* Dans cette s�rie d'exercices:
*	prise en main de C++,
*	diff�rences entre fichiers sources (.cpp) vs fichiers d'en-t�te (.h)
*	linkage entre fichiers .h, .cpp
*	Diff�rences entre d�claration, d�finition, initialisation, affectation
*	D�couverte des variables
*/


/*
* Explications pour exercice 1
* int main() {[...]} est la porte d'entr�e de votre programme C++, c'est l� que tout commence;
* Si int main() est absent de votre code C++, alors le compilateur ne voudra pas de votre code;
* Une et une seule fonction main peut exister dans un projet C++, sinon il y aura erreur de compilation;
* Par convention, main renvoie un int (g�n�ralement, on a [return 0;]);
*/

/*
* Pour l'exercice 3
* Dans main.cpp, s'il n'y a pas de d�claration ou d'inclusion d'une 
* d�claration de bonjour(), alors la compilation ne s'effectue pas.
* Deux m�thodes: 
*	d�clarer bonjour() dans main.cpp
*	#include "fonction.h" dans main.cpp et d�clarer bonjour() dans fonction.h
*/

#include <iostream>
#include "fonction.h"

int main() {
	//bonjour();
	exerciceA();
	double x = 3.14;
	std::cout << x;
	double y;
	//std::cout << y; // le compilateur affichera une erreur, y non initialis�, on ne peut pas l'utiliser ici.
	//y = 3.14;

	//Les variables constantes ont besoin d'�tre initialis�es d�s leur d�claration.
	//Apr�s elles ne peuvent plus �tre chang�es
	const double pi = 3.14159;
	//pi = 3.14; //erreur

	return 0;
}