#include<iostream>
#include<string>
#include "fonction.h"
using namespace std;

//#define PI 3.14159

//fonction bonjour() exercices 1->3
/*void bonjour() {
	cout << "Entrez votre prenom :";
	string prenom;
	cin >> prenom;
	cout << "Bonjour " << prenom << "\n";
}
*/

//fonction exerciceA() exercice 5
/*
* But: manier les fonctions d'entr�e/sortie du C++
* Point int�ressant: chercher � d�finir les variables le plus tard possible 
* 
*/
void exerciceA() {
	//Version C
	/*
	int r; double p, s;
	printf("donnez le rayon entier d�un cercle:");
	scanf("%d",&r);
	p=2*PI*r;
	s=PI*r*r;
	printf("le cercle de rayon %d ",r);
	printf("a un perimetre de %f et une surface de %f\n",p,s);
	*/

	//Version C++
	const double PI = 3.14159;
	// Version alternative qui marche � partir de C++ 11:
	// const auto PI = 3.14159;
	/*
	* auto est un mot cl� r�serv� en C++ (C+11 et plus) qui permet,
	* lors de la compilation, de d�terminer le type de la variable d�finie avec ce type auto
	*/
	int r;
	std::cout << "Saisissez le rayon d'un cercle (entier): ";
	std::cin >> r;

	/*
	* double p = 2 * PI * r;
	* double s = PI * r * r;
	*/

	/*
	* On peut faire de m�me avec auto pour la circonf�rence et la surface
	*/
	auto p = 2 * PI * r;
	auto s = PI * r * r;
	// std::cout peut �tre r�parti sur plusieurs lignes pour faciliter la lecture dans votre IDE
	std::cout << "Votre cercle de rayon " << r 
		<< "a une circonf�rence de " << p 
		<< "et une surface de "		<< s;
	
}
