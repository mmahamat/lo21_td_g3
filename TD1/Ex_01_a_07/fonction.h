/*
* Remarques exercice 2:
*	Si fonction.h n'est pas inclus dans fonction.cpp,
*		alors compilation OK
*	Si inclus dans fonction.cpp, alors erreur de compilation car le 
*		compilateur cherche "Une", "Universit�", etc. dans la
*		liste des instructions, fonctions, etc. qui existent
*		==> Erreurs de syntaxe pour le compilateur
*/

//Une universit� de technologie,
//est un �tablissement � caract�re scientifique, culturel et professionnel
//qui a pour mission principale la formation des ing�nieurs,
//le d�veloppement de la recherche et de la technologie.

/*
* Pour l'exercice 3: d�claration de bonjour ici:
* Pour l'exercice 4: l'inclusion conditionnelle peut �tre effectu�e de deux fa�ons:
*	utilisation de ifndef MYVAR ==> define MYVAR ==> endif
*	pragma once
* pragma once est une instruction pr�processeur d'inclusion conditionnelle non-conventionnelle 
* qui n'est pas forc�ment prise en charge par tous les compilateurs C++ 
* mais qui remplit le m�me devoir que ifndef... define ... endif
*/ 

#ifndef MY_VAR
#define	MY_VAR

#include "essai.h"

//void bonjour();
void exerciceA();

#endif // !MY_VAR



