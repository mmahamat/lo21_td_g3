#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H
#include <iostream>
#include <string>
#include "timing.h"
#include <vector>
namespace TIME {
	class Evt1j {
	private:
		Date date;
		std::string sujet;
	public:
		Evt1j(const Date& d, const std::string& s) :date(d), sujet(s) 
		{ std::cout << "construction evt1j\n"; }
		virtual ~Evt1j() { std::cout << "destruction evt1j\n"; }
		const std::string& getDescription() const { return sujet; }
		const Date& getDate() const { return date; }
		virtual void afficher(std::ostream& f = std::cout) const;
	};


	class Evt1jDur : public Evt1j
	{
		Horaire m_horaire;
		Duree m_duree;

	public: 
		Evt1jDur(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur) :Evt1j(d, s),m_horaire(h), m_duree(dur)
		{
			std::cout << "evt1jdur\n";
		};
		~Evt1jDur() override { std::cout << "destruction evt1jdur\n"; }
		const Horaire& getHoraire() const {
			return m_horaire;
		}
		const Duree& getDuree() const {
			return m_duree;
		}
		void afficher(std::ostream& f = std::cout) const override;
	};


	class Rdv : public Evt1jDur
	{
		std::string lieu;
		std::string personnes;
	public:
		Rdv(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur,
			const std::string& pers,
			const std::string& li) : Evt1jDur(d, s, h, dur), lieu(li), personnes(pers)
		{
			std::cout << "Rdv\n";
		};
		~Rdv() { std::cout << "destruction Rdv\n"; }
		const std::string& getLieu() const {
			return lieu;
		}
		const std::string& getPersonnes() const {
			return personnes;
		}
		void afficher(std::ostream& f = std::cout) const override;
	};
	class Agenda
	{
	private:
		std::vector<Evt1j*> m_events;
		Agenda& operator=(const Agenda& t_agenda) = delete; //operateur d'affectation
		Agenda(const Agenda& t_agenda) = delete; //constructeur par recopie
	public:
		Agenda() = default;
		//Agenda() : m_events() {};
		virtual ~Agenda() {};
		Agenda& operator<<(Evt1j& e);
		void afficher(std::ostream& f = std::cout) const;

	};
}

std::ostream& operator<<(std::ostream& f, const TIME::Evt1j& t_evt1j);
#endif