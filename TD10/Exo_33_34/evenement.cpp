#include "evenement.h"

void TIME::Evt1j::afficher(std::ostream&f ) const {
	f << "***** Evt ********" << "\n" << "Date=" << date << " sujet=" << sujet << "\n";
}

void TIME::Evt1jDur::afficher(std::ostream& f) const {
	Evt1j::afficher(f);
	f << "horaire de debut: " << getHoraire() << ", duree : " << getDuree() << "\n";
}

void TIME::Rdv::afficher(std::ostream&f) const
{
	Evt1jDur::afficher(f);
	f << "personnes = " << getPersonnes() << ", lieu = " << getLieu() << "\n";
}

TIME::Agenda& TIME::Agenda::operator<<(TIME::Evt1j& e)
{
	m_events.push_back(&e); //stockage de l'adresse
	return *this; //retour de l'agenda
}

void TIME::Agenda::afficher(std::ostream& f) const
{
	f << "Voici votre agenda:\n";
	f << "----------------------\n";
	for (size_t i = 0; i < m_events.size(); i++)
		m_events[i]->afficher();
	f << "---------------------\n";
	f << "Fin de votre agenda.\n";
}

std::ostream& operator<<(std::ostream& f, const TIME::Evt1j& t_evt1j)
{
	t_evt1j.afficher(f);
	return f;
}

