#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H
#include <iostream>
#include <string>
#include "timing.h"
#include <vector>
namespace TIME {
	class Evt
	{
	private:
		std::string sujet;
	public:
		Evt(const std::string& t_s) :sujet(t_s) {};
		virtual ~Evt() {};
		virtual void afficher(std::ostream& f = std::cout) const = 0;// methode virtuelle pure
		// a definir dans les classes filles
		const std::string getDescription() const { return sujet; };
		virtual Evt* duplicate() const = 0;
	};
	class Evt1j  : public Evt{
	private:
		Date date;
		//std::string sujet;
	public:
		Evt1j(const Date& d, const std::string& s) :Evt(s),date(d) 
		{ std::cout << "construction evt1j\n"; }
		virtual ~Evt1j() { std::cout << "destruction evt1j\n"; }
		//const std::string& getDescription() const { return sujet; }
		const Date& getDate() const { return date; }
		virtual void afficher(std::ostream& f = std::cout) const;
		Evt1j* duplicate() const override;
	};
	class EvtPj : public Evt
	{
	private:
		//Date date_debut;
		//Date date_fin;
		Intervalle dates_event;
	public:
		EvtPj(const Date& d1, const Date& d2, const std::string& s) : dates_event(d1, d2), Evt(s) {};
		~EvtPj() {};
		void afficher(std::ostream& f = std::cout) const override;
		EvtPj* duplicate() const override;
	};

	class Evt1jDur : public Evt1j
	{
		Horaire m_horaire;
		Duree m_duree;

	public: 
		Evt1jDur(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur) :Evt1j(d, s),m_horaire(h), m_duree(dur)
		{
			std::cout << "evt1jdur\n";
		};
		~Evt1jDur() override { std::cout << "destruction evt1jdur\n"; }
		const Horaire& getHoraire() const {
			return m_horaire;
		}
		const Duree& getDuree() const {
			return m_duree;
		}
		void afficher(std::ostream& f = std::cout) const override;
		Evt1jDur* duplicate() const override;
	};


	class Rdv : public Evt1jDur
	{
		std::string lieu;
		std::string personnes;
	public:
		Rdv(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur,
			const std::string& pers,
			const std::string& li) : Evt1jDur(d, s, h, dur), lieu(li), personnes(pers)
		{
			std::cout << "Rdv\n";
		};
		~Rdv() { std::cout << "destruction Rdv\n"; }
		const std::string& getLieu() const {
			return lieu;
		}
		const std::string& getPersonnes() const {
			return personnes;
		}
		void afficher(std::ostream& f = std::cout) const override;
		Rdv* duplicate() const override;
	};
	class Agenda
	{
	private:
		std::vector<Evt*> m_events;
		Agenda& operator=(const Agenda& t_agenda) = delete; //operateur d'affectation
		Agenda(const Agenda& t_agenda) = delete; //constructeur par recopie
	public:
		Agenda() = default;
		//Agenda() : m_events() {};
		virtual ~Agenda() {
			for (size_t i = 0; i < m_events.size(); i++)
				delete m_events[i];
		};
		Agenda& operator<<(Evt& e);
		void afficher(std::ostream& f = std::cout) const;
		class iterator : public std::vector<Evt*>::iterator
		{
		public:
			//todo
			Evt& operator*() const
			{ return *std::vector<Evt*>::iterator::operator*(); }
		private:
			friend class Agenda;
			iterator(const std::vector<Evt*>::iterator& it) :
				std::vector<Evt*>::iterator(it) {};
		};
		iterator begin() {
			return iterator(m_events.begin());
		}

		iterator end() { return iterator(m_events.end()); }
		class const_iterator : public std::vector<Evt*>::const_iterator
		{
		public:
			//todo
			const Evt& operator*() const
			{
				return *std::vector<Evt*>::const_iterator::operator*();
			}
		private:
			friend class Agenda;
			const_iterator(const std::vector<Evt*>::const_iterator& it) :
				std::vector<Evt*>::const_iterator(it) {};
		};

		const_iterator cbegin() const
		{
			return const_iterator(m_events.begin());
		}
		const_iterator cend() const
		{
			return const_iterator(m_events.end());
		}
		const_iterator begin() const
		{
			return const_iterator(m_events.begin());
		}
		const_iterator end() const
		{
			return const_iterator(m_events.end());
		}
	};
}

std::ostream& operator<<(std::ostream& f, const TIME::Evt& t_evt);
#endif