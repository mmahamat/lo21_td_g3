#include <iostream>
#include "evenement.h"
int main() {
	using namespace std;
	using namespace TIME;
	//Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	//Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	//e1.afficher();
	//e2.afficher();
	//{ // d�but de bloc
	//		//creation dans l'ordre Evt1j->Evt1jDur->Rdv

	//	Rdv e(Date(11, 11, 2013),
	//		"reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	//		std::cout << "RDV:";
	//	e.afficher();
	//	//destruction dans l'ordre Rdv -> Evt1jdur-> Evt1j
	//} // fin de bloc
	////nouveau bloc
	//{ //probleme de polymorphisme avec le code fait en td ...
	//	Evt1j* my_pt1 = new Rdv(Date(26, 04, 2022),
	//		"reunion LO21", Horaire(11, 14), Duree(12), "Intervenants UV", "bureau");
	//	std::cout << "RDV pointe:";
	//	my_pt1->afficher();
	//	delete my_pt1;
	//}
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree
	(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
		e1.afficher(); e2.afficher(); e3.afficher(); e4.afficher();
	Evt1j * pt1 = &e1; Evt1j * pt2 = &e2; Evt1j * pt3 = &e3; Evt1j * pt4 = &e4;
	pt1->afficher(); pt2->afficher(); pt3->afficher(); pt4->afficher();

	std::cout << "Suite affichage sur flux des differents objets:\n";
	std::cout << e1;
	std::cout << e4;

	std::cout << "essai de l'agenda, version avec Evt abstrait:\n";
	Agenda my_agenda;
	my_agenda << e1;
	my_agenda << e2;
	my_agenda << e3;
	my_agenda << e4;
	my_agenda.afficher();
	std::cout << "essai de creation d'instance d'EvtPj\n";
	EvtPj un_evtpj(Date(29, 7, 2022), Date(4, 8, 2022), "conference");
	un_evtpj.afficher();
	my_agenda << un_evtpj;
	my_agenda.afficher();
	std::cout << "essai de l'iterateur en forward\n";
	for (auto it = my_agenda.begin(); it != my_agenda.end(); ++it)
		std::cout << *it << "\n";
	
	std::cout << "essai de l'iterateur en backward\n";
	auto itbackwards = my_agenda.end();
	for (itbackwards; itbackwards != my_agenda.begin(); --itbackwards)
	{
		if(itbackwards != my_agenda.end())
			std::cout << *itbackwards << "\n";
	}
	std::cout << *itbackwards << "\n";
	return 0;
}