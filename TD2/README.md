# TD2 - Organisation du répertoire

* Ex_11 : contient la correction de l'exercice 11 qui a été réalisé au vidéo-projecteur
* Ex_12_13_14: contient la correction des exercices 12, 13, 14. L'exercice 14 a été corrigé à l'oral.
* Ex_15: contient la correction de l'exercice 15 qui avait été corrigé au tableau.
* Ex_16: correction de l'exercice 16.
* Ex_16: correction alternative de l'exercice 16 (qui considère cette fois un fichier .h et un source.cpp)
* Ex_17_18: correction des exercices 17, 18 et _19_.
