#include <iostream>
#include <string>

//

int main()
{
	//double& d1 = 36; // Nope, une r�f�rence de type T a besoin d'une lvalue (variable, fonction qui renvoie une lvalue, membre d'une classe)
	double d2 = 36; // oui
	double& ref = d2; // Ok
	std::cout << "maref= " << ref << "\n";
	ref = 4; // revient � faire d2 = 4, chez certains d'entre vous, cette ligne ne semble pas marcher
	// Si �a plante, quelle est l'erreur que votre compilateur renvoie?
	std::cout << "valeur de maref= " << ref << "\n";
	double* myptdb = &d2;
	*myptdb = 44312;
	std::cout << "myptdb= " << *myptdb << "\n";
	const double d3 = 36; //ok
	const double& d4 = 36; //ok, on donne un espace m�moire pour y stocker la valeur 36, puis on assigne � cet espace m�moire une ref pour y acc�der
	const double& d5 = d2; //ok, on peut r�f�rencer une variable de type T avec une r�f�rence const T
	//d5 = 21; //Non, une r�f�rence const ne permet pas de changer la variable r�f�renc�e (elle est consid�r�e comme const du pdv de d5)
	const double& d6 = d3; //Oui ok
	//double& ref2 = d6; //non car une ref T ne peut pas r�f�rencer une const T var, on perd la condition de constance
	int i = 4; // ok
	//double& d7 = i; //non, r�f�rences T doivent r�f�rencer des variables de type T
	const double& d8 = i; // Ok car une r�f�rence const r�f�rence la conversion de i vers double, mais non i en elle m�me
	//d8 = 3 ; // non
	std::cout << "i avant modif " << i << "\n";
	i = 5;
	std::cout << "\n i modif directement= " << i;
	std::cout << "\n i via la r�f�rence d8= " << d8;

	//extras pour montrer les diff�rences d'�criture avec un pointeur et une r�f�rence
	int myvar = 007;
	int& myref = myvar; // definition d�une r�f�rence
	int* mypt = &myvar; // approche avec un pointeur o� on sp�cifie l�adresse
	std::cout << myref; //afficher ce que poss�de la reference
	std::cout << *mypt; //afficher ce que poss�de le pointeur (l�op�rateur * permet de d�r�ferencer le pointeur et d�acc�der � la valeur)
	myref = 42; // myvar = 42


	return 0;
}