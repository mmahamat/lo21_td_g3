#pragma once
#include <string>
namespace Ex1718
{
	struct personne {
		char nom[30]; //version char*
		//std::string nom; //version avec <string>
		unsigned int age;
	};

	void raz(personne& myp);
	void affiche_struct(personne& myp);
	void affiche_tab(personne* montab, int tabsize);
	void init_struct(personne& perso, const char* name, int ageinit); //version C, const char est important!
	//void init_struct(personne& perso, const std::string name, int ageinit); //version C++ avec <string>
	void copy_struct(personne& persA, personne& persB); //persA = dest, persB = source
	void copy_tab(personne tabA[], personne tabB[], int taille); // tabA = dest, tabB = source

	// Une version plus sure des copy_struct/copy_tab serait d'avoir la variable source
	// comme �tant const, pour �viter toute modification de la source
	//void copy_struct_safe(personne& persA, const personne& persB); //persA = dest, persB = source
	//void copy_tab_safe(personne tabA[], const personne tabB[], int taille); // tabA = dest, tabB = source
	//La d�finition de ces fonctions "safe" est la m�me que celles non safe

	void test_struct_personne(); //tester toutes les fonctions de l'exercice 17/18

}


namespace Ex19
{
	// structure de l'exercice 19
	struct compte
	{
		std::string id;
		int solde;
	};

	int& operation(compte* tableau, std::string identifiant);
	void essai_comptes();
}
