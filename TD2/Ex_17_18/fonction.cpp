#include "fonction.h"
#include <iostream>
#include <string>
void Ex1718::raz(Ex1718::personne& myp)
{
	myp.age = 0;
	*myp.nom = '\0';
	//myp.nom = ""; 
	
	/*
	* avec <string> on peut directement initialiser le champ nom avec la
	* cha�ne vide "".
	*/
}

void Ex1718::affiche_struct(Ex1718::personne& myp)
{
	std::cout << "name: " << myp.nom << ", age:" << myp.age << "\n";
}

void Ex1718::affiche_tab(Ex1718::personne* montab, int tabsize)
{
	for (int i = 0; i < tabsize; i++)
	{
		//affiche_struct(*montab);
		//montab++;
		Ex1718::affiche_struct(montab[i]);
	}
}

void Ex1718::init_struct(Ex1718::personne& perso, const char* name, int ageinit) //version C
{
	perso.age = ageinit;
	char* ptnom = perso.nom;
	int i;
	for (i = 0; name[i] != '\0'; i++)
	{
		ptnom[i] = name[i];
	} 
	ptnom[i] = '\0'; //fin de cha�ne de caract�re
	
	//une version moins barbare (et plus simple) que celle qui suit 

	/*while (*name)
	{
		*ptnom = *name;
		++name;
		++ptnom;
	}
		*ptnom = '\0';
	*/ 
	//une version barbare de copie de cha�ne de caract�res en C
	//en utilisant exclusivement des pointeurs



}

//
//void Ex1718::init_struct(Ex1718::personne& perso, const std::string name, int ageinit) 
//{
//  //version C++ avec la librairie <string>
//	perso.age = ageinit;
//	perso.nom = name;
//
//}

void Ex1718::copy_struct(Ex1718::personne& persA, Ex1718::personne& persB) //personne A re�oit personne B
{
	persA = persB;
}

void Ex1718::copy_tab(Ex1718::personne tabA[], Ex1718::personne tabB[], int taille)
{
	for (int i = 0; i < taille; i++)
		tabA[i] = tabB[i];

}

void Ex1718::test_struct_personne() {
	personne p1;
	affiche_struct(p1); //on peut afficher une structure non initialis�e, 
						//mais les champs auront des valeurs al�atoires
	raz(p1);
	affiche_struct(p1);
	std::cout << "RAS\n";
	personne p2;
	init_struct(p2, "Azem", 30);
	affiche_struct(p2);
	copy_struct(p1, p2);
	std::cout << "affichage p1 copiee via p2:\n";
	affiche_struct(p1);
	personne listeA[5] = { {"Azem", 30}, {"Josue", 50}, {"Elidibus", 26}, {"Atlassian", 44}, {"Jira", 22} };
	std::cout << "affichage listeA:\n";
	affiche_tab(listeA, 5);

	personne listeB[5];
	copy_tab(listeB, listeA, 5);
	std::cout << "affichage listeB copiee via listeA:\n";
	affiche_tab(listeB, 5);
}

//Exercice 19, travail sur les retours par r�f�rence

int& Ex19::operation(compte* tableau, std::string identifiant)
{
	while (tableau->id != identifiant)
		tableau++;
	return tableau->solde;

}

void Ex19::essai_comptes() {
	compte tab[4] = { {"courant", 0},{"codevi", 1500 },
	{"epargne", 200 }, { "cel", 300 } };
	operation(tab, "courant") = 100;
	operation(tab, "codevi") += 100;
	operation(tab, "cel") -= 50;
	//operation(tab, "courant") *= 20; //jackpot
	for (int i = 0; i < 4; i++) 
		std::cout << tab[i].id << " : " << tab[i].solde << "\n";
}