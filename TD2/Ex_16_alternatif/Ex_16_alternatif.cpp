#include "fonction.h"
#include <array>
using namespace std;

/*
* Variante de l'exercice 16, qui montre qu'on peut effectivement
* définir les fonctions constexpr hors du fichier source
* où se trouve la fonction main()
*/

int main() {
	constexpr int N = getNumber();
	array<int, calcul(N)> tableau;
	return 0;
}