#pragma once

//Les fonctions constexpr sont implicitement inlines
//On peut donc les d�finir dans un header qui sera inclus
//dans le fichier source .cpp

constexpr int calcul(int x) { return 2 * x + 1; }
constexpr int getNumber() { return 3; }