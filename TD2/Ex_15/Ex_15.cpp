#include <iostream>

void essai_alloc() {
    int* pt_int;
    double* pt_double;
    pt_int = (int*)malloc(sizeof(int));
    pt_double = (double*)malloc(sizeof(double) * 100);
    free(pt_int);
    free(pt_double);
}

void essai_alloc_cpp() {
    int* pt_int;
    double* pt_double;
    pt_int = new int;
    pt_double = new double[100];
    delete pt_int;
    delete[] pt_double;
}

int main()
{
    std::cout << "Exercice 15, allocation version c++\n";
    essai_alloc_cpp();
    return 0;
}

