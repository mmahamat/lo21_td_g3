#include <iostream>
#include "fonction.h"
int main()
{
    int myInt = 3;
    int& myref = myInt;

    std::cout << "adr myref: " << &myref;
    //la référence n'ayant pas d'adresse pour elle même, c'est l'adresse de myInt qui est affichée
    std::cout << "\nadr myInt: " << &myInt <<'\n';

    Ex12::test_inverse();
    Ex13::test_struct_essai();
    Ex14::essai_init();
    return 0;
}


