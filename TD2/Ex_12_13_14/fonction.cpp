#include "fonction.h"
#include <iostream>

void Ex12::inverse(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void Ex12::inverse(int& a, int& b)
{
	int tmp = a;
	a = b;
	b = tmp;
}


void Ex12::test_inverse()
{
	int x = 42, y = 007;
	Ex12::inverse(&x, &y); //appel passage par pointeurs
	std::cout << "x=" << x << ", y=" << y << "\n";
	Ex12::inverse(x, y); //passage par r�f�rence
	std::cout << "x=" << x << ", y=" << y << "\n";
}


void Ex13::miseAZero(essai* e)
{
	e->n = 0; // (*e).n = 0
	//la fl�che est un sucre syntaxique qui simplifie l'�criture
	e->x = 0.0;
}

void Ex13::miseAZero(essai& e)
{
	e.n = 0;
	e.x = 0.0;
}

void Ex13::test_struct_essai()
{
	Ex13::essai my_e;
	Ex13::miseAZero(my_e); //passage par r�f�rence
	std::cout << "n member= " << my_e.n << " and x member= " << my_e.x << "\n";
	my_e.n = 42;
	my_e.x = static_cast<float>(07.07);
	std::cout << "n member= " << my_e.n << " and x member= " << my_e.x << "\n";
	Ex13::miseAZero(&my_e);
	std::cout << "n member= " << my_e.n << " and x member= " << my_e.x << "\n";
}


void Ex14::init(point* pt, int _x, int _y, int _z) 
{
	//Quand on d�finit une fonction avec des arguments par d�faut,
	//il ne faut pas r��crire la valeur par d�faut de chaque argument,
	//sinon le compilateur consid�re que vous red�finissez les arguments par d�faut,
	//et donc erreur de compilation.
	pt->x = _x;
	pt->y = _y;
	pt->z = _z;
}
void Ex14::essai_init()
{
	//passages par adresse effectu�s ici.
	Ex14::point p;
	init(&p);
	std::cout << "p.x = " <<p.x <<", p.y = "<< p.y<<", p.z = "<<p.z <<"\n";
	init(&p, 1);
	std::cout << "p.x = " << p.x << ", p.y = " << p.y << ", p.z = " << p.z << "\n";
	init(&p, 1, 2);
	std::cout << "p.x = " << p.x << ", p.y = " << p.y << ", p.z = " << p.z << "\n";
	init(&p, 1, 2, 3);
	std::cout << "p.x = " << p.x << ", p.y = " << p.y << ", p.z = " << p.z << "\n";
}