#pragma once

//Exercice 12
namespace Ex12
{
	void inverse(int* a, int* b);
	void inverse(int& a, int& b);
	void test_inverse();

}
//end namespace Ex12

//Exercice 13
namespace Ex13
{
	struct essai
	{
		int n;
		float x;
	};

	void miseAZero(essai* e);
	void miseAZero(essai& e);
	void test_struct_essai();
}
//end namespace Ex13

//Exercice 14
namespace Ex14
{
	//d�terminer comment avoir des arguments par d�faut en C++!
	struct point {
		int x;
		int y;
		int z;
	};

	void init(point* pt, int _x=0, int _y=0, int _z=0); // arguments par d�faut!
	//void init(point* pt, int _x, int _y, int _z);
	//void init(point* pt, int _x, int _y);
	//void init(point* pt, int _x);
	//void init(point* pt);
	void essai_init();
}// end namespace Ex14
