#if !defined(_Container_T_H)
#define _Container_T_H
#include<string>
#include<stdexcept>
namespace TD {
	class ContainerException : public std::exception {
	protected:
		std::string info;
	public:
		ContainerException(const std::string& i = "") noexcept :info(i) {}
		const char* what() const noexcept { return info.c_str(); }
		~ContainerException()noexcept {}
	};

	template<class T> 
	class Container
	{
	protected:
		size_t nbElems;
	public:
		Container(size_t n) :nbElems(n) {};
		virtual ~Container() {};
		size_t size() const { return nbElems; };
		bool empty() const { return nbElems == 0; };
		virtual T& element(size_t i) = 0; //methode virtuelle pure
		virtual const T& element(size_t i) const = 0;
		virtual T& front();
		virtual const T& front() const;
		virtual T& back();
		virtual const T& back() const;
		virtual void push_back(const T& x) = 0;
		virtual void pop_back() = 0;
		virtual void clear()
		{
			while (!empty())
			{
				pop_back();
			}
		}

	};

	template<class T>
	T& Container<T>::front()
	{
		if (!empty()) return element(0);
		else throw ContainerException("ContainerException: impossible to retrieve first element");
	}

	template<class T> 
	const T& Container<T>::front() const
	{
		if (!empty()) return element(0);
		else throw ContainerException("ContainerException: impossible to retrieve first element");
	}
	template<class T>
	T& Container<T>::back()
	{
		if (!empty()) return element(nbElems -1);
		else throw ContainerException("ContainerException: impossible to retrieve last element");
	}
	template<class T> 
	const T& Container<T>::back() const
	{
		if (!empty()) return element(nbElems - 1);
		else throw ContainerException("ContainerException: impossible to retrieve last element");
	};

}//fin du namespace
#endif