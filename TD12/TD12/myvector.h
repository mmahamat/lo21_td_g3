#ifndef MY_VECT
#define MY_VECT

#include "container.h"
namespace TD {

	template<class T>
	class Vector : public TD::Container<T>
	{
	private:
		T* values;
		size_t size_values;
	public:
		Vector(size_t n = 0, const T& the_init = T()); //ctor normal
		Vector(const Vector<T>& tab); //ctor recopie
		~Vector() { delete[] values; } //dtor
		T& element(size_t i) override; //ieme element
		const T& element(size_t i) const override;
		void push_back(const T& x) override;
		void pop_back() override;
		T& operator[](size_t i) { return element(i); };

		const T& operator[](size_t i) const { return element(i); };
		Vector<T>& operator=(const Vector<T>& v); //surcharge operator=


	};//fin classe Vector<T>

	template<class T> 
	Vector<T>::Vector(size_t n, const T& the_init) :Container<T>(n),size_values(n),
		values(new T[n]) {
		for (size_t i = 0; i < Container<T>::nbElems; i++)
		{
			values[i] = the_init;
		}
	}

	template<class T>
	Vector<T>::Vector(const Vector<T>& tab):Container<T>(tab.nbElems),values(new T[tab.size()]),size_values(tab.size())
	//ctor copy
	{
		for (size_t i = 0; i < this->size(); i++)
			values[i] = tab.values[i];
	}

	template<class T>
	T& Vector<T>::element(size_t i)
	{
		if (i < Container<T>::nbElems) return values[i];
		else throw ContainerException("VectorError: index not in range");
		
	}; //ieme element
	template<class T>
	const T& Vector<T>::element(size_t i) const
	{
		if (i < Container<T>::nbElems) return values[i];
		else throw ContainerException("VectorError: index not in range");

	}; //ieme element

	template<class T>
	Vector<T>& Vector<T>::operator=(const Vector<T>& v) //operator=
	{
		if (this != &v)
		{
			T* new_v = new T[v.nbElems];
			for (size_t i = 0; i < v.nbElems; i++)
			{
				new_v[i] = v.values[i];
			}
			Container<T>::nbElems = v.nbElems; //mise � jour du nbElems de la partie Container
			size_values = v.nbElems;
			T* old_values = values;
			values = new_v; 
			delete[] old_values;
		}
		return *this;
	}

	template<class T>
	void Vector<T>::push_back(const T& x)
	{
		if (this->nbElems == size_values)
		{
			T* new_values = new T[2 * size_values + 1];
			for (size_t i = 0; i < this->nbElems; i++)
				new_values[i] = values[i]; //recopie des anciennes valeurs
			T* old_values = values;
			size_values = 2 * size_values + 1;
			values = new_values;
			delete[] old_values;
		}
		values[this->nbElems++] = x;
	};

	template<class T>
	void Vector<T>::pop_back()
	{
		if (!this->empty())
		{
			this->nbElems--;
		}
		else throw ContainerException("VectorError: empty vector");
	}

}//end du namespace
#endif