# LO21_TD_G3

Code source des tds de LO21 du groupe du mardi matin (G3) lors du semestre de printemps 2022.
Chaque Td possède sa correction dans le dossier dédié.

Le dossier `misc/` possède différents éléments complémentaires présentés au début de chaque TD, ou envoyés après chaque TD.
